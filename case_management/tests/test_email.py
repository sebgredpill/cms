from os.path import basename

from odoo.tests import tagged, common

from ..util.email import send_email, valid_email


@tagged('casemanagement_failing')
class TestEmailSending(common.TransactionCase):

    def test_send_email(self):
        applicant = Applicant("Applicant Name", "applicant@example.com")
        email_content = """\
                        Please note that you are lacking necessary documents below to complete your application:
                        
                        * Residence permit
                        * Consent to process your data
                        
                        Best regards,
                        The Internship Team
                        """
        subject = "Hello from TestEmailSending test class!"

        # Attach test employers CSV file
        file_path = '/mnt/extra-addons/case_management/static/file/employers.csv'
        file_dict = dict()
        with open(file_path, 'rb') as file:
            file_dict['binary'] = file.read()
            file_dict['name'] = basename(file_path)

        result = send_email(applicant, email_content, subject, file_dict, 'js@jobbspranget.se')

        self.assertTrue(result)

    def test_valid_email(self):
        self.assertTrue(valid_email('m@gmail.com'))
        self.assertTrue(valid_email('maaaaaa.adsfadsfa@bbc.co.uk'))
        self.assertTrue(valid_email('agent.smith@gov.bbc.co.uk'))
        self.assertTrue(valid_email('agent+smith@gov.bbc.co.uk'))
        self.assertTrue(valid_email('agent%smith@gov.bbc.co.uk'))
        self.assertTrue(valid_email('agent_smith@gov.bbc.co.uk'))

        self.assertFalse(valid_email('.smith@gov.bbc.co.uk'))
        self.assertFalse(valid_email('hello-there.yahoo.com'))
        self.assertFalse(valid_email('@hello-there.yahoo.com'))
        self.assertFalse(valid_email('hello-there@com'))
        self.assertFalse(valid_email('hello'))
        self.assertFalse(valid_email(''))
        self.assertFalse(valid_email(None))


class Applicant:
    """
    Mock Applicant to avoid fetching from DB
    """

    def __init__(self, name, email):
        self.name = name
        self.email = email
