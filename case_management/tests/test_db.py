import logging
import os
from datetime import datetime, timedelta

from odoo.tests import common, tagged

_logger = logging.getLogger(__name__)

SOLID_SERVER_URL = os.environ.get("SOLID_SERVER_URL", "http://localhost:3000")


@tagged('casemanagement')
class TestsCaseManagementModels(common.TransactionCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def setUp(self):
        super(TestsCaseManagementModels, self).setUp()

    def test_notification(self):
        # Data from Solid notification posted to notification webhook as JSON
        json_data = {
            '@context': ['https://www.w3.org/ns/activitystreams', 'https://www.w3.org/ns/solid/notification/v1'],
            'id': 'urn:uuid:7ef726fb-90f8-4b00-ad3e-3c34b814dd94',
            'type': ['Create'],
            'object': {'id': f'{SOLID_SERVER_URL}/source/foo/baa'},
            'published': '2022-01-19T14:11:44.745Z',
            'unsubscribe_endpoint': f'{SOLID_SERVER_URL}/webhook/'
                                    'http%3A%2F%2Flocalhost%3A3000%2Fsource%2Ffoo%2Fbaa'
                                    '~~~'
                                    'fdf77922-b433-4b86-a53e-8302cfaad268'}

        published_datetime_str = json_data.get('published')
        if published_datetime_str:
            published_datetime = datetime.strptime(published_datetime_str, '%Y-%m-%dT%H:%M:%S.%fZ')
        else:
            published_datetime = datetime.now()

        result = self.env['casemanagement.notification'].create({
            'urn_uuid': json_data['id'],
            'type': json_data['type'],
            'object': json_data.get('object'),
            'subscription_target': json_data.get('object', dict()).get('id'),
            'unsubscribe_endpoint': json_data.get('unsubscribe_endpoint'),
            'published': published_datetime
        })

        self.assertTrue(result)

        fetched_notification = self.env['casemanagement.notification'].search([]).read()

        self.assertTrue(fetched_notification)
        self.assertTrue(len(fetched_notification) >= 1)

        _logger.info("==================================================================")
        _logger.info(fetched_notification)
        _logger.info("==================================================================")

        # Create using custom model method
        created_with_custom_method = self.env['casemanagement.notification'].create_from_notification_json(
            json_data
        )

        self.assertTrue(created_with_custom_method)

        fetched_notification = self.env['casemanagement.notification'].search([]).read()

        self.assertTrue(len(fetched_notification) >= 2)

        _logger.info("==================================================================")
        _logger.info(fetched_notification)
        _logger.info("==================================================================")

        # Now create two at a time
        created_with_custom_method = self.env['casemanagement.notification'].create_from_notification_json(
            [json_data, json_data]
        )

        self.assertTrue(created_with_custom_method)

        fetched_notification = self.env['casemanagement.notification'].search([]).read()

        self.assertTrue(len(fetched_notification) >= 4)

        _logger.info("==================================================================")
        _logger.info(fetched_notification)
        _logger.info("==================================================================")

    def test_add_comment(self):
        """
        Short documentation on how to work with linked records:

        (0, 0,  { values })    link to a new record that needs to be created with the given values dictionary
        (1, ID, { values })    update the linked record with id = ID (write *values* on it)
        (2, ID)                remove and delete the linked record with id = ID (calls unlink on ID,
                               that will delete the object completely, and the link to it as well)
        (3, ID)                cut the link to the linked record with id = ID (delete the relationship between the two
                               objects but does not delete the target object itself)
        (4, ID)                link to existing record with id = ID (adds a relationship)
        (5)                    unlink all (like using (3,ID) for all linked records)
        (6, 0, [IDs])          replace the list of linked IDs (like using (5) then (4,ID) for each ID in the
                               list of IDs)
        :return:
        """
        comment_text = 'Here is a test comment'
        # Create new comment as a part of Employer
        created_employer = self.env['casemanagement.employer'].create({
            'name': 'TEST employer',
            'org_number': '898989-1234',
            'email': 'test@testemployer.org',
            'comment_ids': [(0, 0, {'comment_text': f'{comment_text} 1'})],
        })
        self.assertTrue(created_employer)
        self.assertTrue(created_employer.comment_ids)
        self.assertTrue(len(created_employer.comment_ids) == 1)

        # Add a new comment to an existing Employer
        created_employer.write({'comment_ids': [(0, 0, {'comment_text': f'{comment_text} 2'})]})

        fetched_employer = self.env['casemanagement.employer'].browse([created_employer.id])
        self.assertTrue(fetched_employer)
        self.assertTrue(fetched_employer.comment_ids)
        self.assertTrue(len(fetched_employer.comment_ids) == 2)

        _logger.info("==================================================================")
        _logger.info(fetched_employer.read())

        fetched_comments = fetched_employer.comment_ids
        for c in fetched_comments:
            _logger.info("==================================================================")
            _logger.info(c.read())

        # Update existing comment
        updated_comment_text = f'{comment_text} 2 UPDATED'
        created_employer.write({'comment_ids': [(1,
                                                 created_employer.comment_ids[-1].id,
                                                 {'comment_text': updated_comment_text})]})
        self.assertEqual(updated_comment_text, fetched_comments[-1][0].comment_text)

        # Remove all comments
        created_employer.write({'comment_ids': [(2, c.id) for c in fetched_comments]})
        fetched_employer = self.env['casemanagement.employer'].browse([created_employer.id])
        self.assertFalse(fetched_employer.comment_ids)

    def test_campaign_create_union_approvals(self):
        """
        Campaign model, method create_union_approvals()
        """
        # Employers
        created_employers = self.env['casemanagement.employer'].create([
            {
                'name': 'TEST employer 1',
                'org_number': '898989-1234',
                'email': 'test@testemployer1.org'
            },
            {
                'name': 'TEST employer 2',
                'org_number': '898989-1235',
                'email': 'test@testemployer2.org'
            },
            {
                'name': 'TEST employer 3',
                'org_number': '898989-1236',
                'email': 'test@testemployer3.org'
            },
        ])
        # Campaign
        created_campaign = self.env['casemanagement.campaign'].create(
            {
                'date_end': datetime.now().date(),
                'employer_ids': [(6, 0, [e.id for e in created_employers])]
            })

        # Create Unions if no existing
        existing_unions = self.env['casemanagement.union'].search([])
        if not existing_unions:
            existing_unions = self.env['casemanagement.union'].create([
                {
                    'name': 'Akavia',
                    'email': 'noreply@jobtech-akavia.se'
                },
                {
                    'name': 'Jusek',
                    'email': 'noreply@jobtech-jusek.se'
                },
                {
                    'name': 'Sveriges Ingenjörer',
                    'email': 'noreply@jobtech-sako.se'
                },
                {
                    'name': 'Sveriges Arkitekter',
                    'email': 'noreply@jobtech-sveark.se'
                },
                {
                    'name': 'Naturvetarna',
                    'email': 'noreply@jobtech-naturvetarna.se'
                }
            ])

        existing_approvals = self.env['casemanagement.approval'].search([
            ['employer_id', 'in', [e.id for e in created_employers]]
        ])
        self.assertFalse(existing_approvals)

        # Create Approval records for all Campaign.employer_ids and all existing Unions
        guid_mapping = created_campaign.create_union_approvals()
        self.assertIsInstance(guid_mapping, list)
        self.assertTrue(len(guid_mapping) == len(existing_unions))

        created_approvals = self.env['casemanagement.approval'].search([
            ['employer_id', 'in', [e.id for e in created_employers]]
        ])

        _logger.info('=============================== CREATED APPROVALS ==============================')
        _logger.info(created_approvals)
        _logger.info('================================================================================')

        self.assertTrue(len(created_approvals) == len(created_employers)*len(existing_unions))

        # Employers are not approved by any Union yet
        for e in created_employers:
            _logger.info(f'Employer "{e.name}" SHOULD not be approved, but waiting')
            self.assertTrue(e.union_approved == 'waiting')

        # Sign Approvals, now Employers should be approved
        created_approvals.sign_approval('19890101-0001')

        # Employers should now be approved
        for e in created_employers:
            _logger.info(f'Employer "{e.name}" SHOULD be approved')
            # for a in e.approval_ids:
            #     _logger.info(f'UnionApproval {a.employer_id.name} {a.id}, status {a.status},'
            #                  f' valid_to_date {a.valid_to_date}, union_approved {e.union_approved}')
            self.assertTrue(e.union_approved == 'ok')

        _logger.info('================================================================================')

    def test_campaign_remove_old_data(self):
        """
        Campaign model, method remove_old_data()
        """
        # Employers
        created_employers = self.env['casemanagement.employer'].create([
            {
                'name': 'TEST employer 1',
                'org_number': '898989-1234',
                'email': 'test@testemployer1.org'
            },
            {
                'name': 'TEST employer 2',
                'org_number': '898989-1235',
                'email': 'test@testemployer2.org'
            },
            {
                'name': 'TEST employer 3',
                'org_number': '898989-1236',
                'email': 'test@testemployer3.org'
            },
        ])
        # Campaign, ends in 7 days from now
        created_campaign = self.env['casemanagement.campaign'].create(
            {
                'date_end': datetime.now().date() + timedelta(days=7),
                'employer_ids': [(6, 0, [e.id for e in created_employers])]
            })
        today = datetime.now().date() + timedelta(days=80)

        # Try to delete old data, nothing should be deleted
        result = self.env['casemanagement.campaign'].remove_old_data(today=today, domain=['name', 'like', 'TEST '])
        self.assertTrue(result[0] == 0, 'Removed Applicants should be 0')
        self.assertTrue(result[1] == 0, 'Removed Applications should be 0')
        _logger.info('=============================== REMOVE OLD DATA 1 ==============================')

        # Create Applicants
        created_applicants = self.env['casemanagement.applicant'].create([
            {
                'first_name': 'TEST 1',
                'last_name': 'LASTNAME',
                'personal_number': '898989-1234',
                'email': 'test1@domain.org'
            },
            {
                'first_name': 'TEST 2',
                'last_name': 'LASTNAME',
                'personal_number': '898989-1235',
                'email': 'test2@domain.org'
            },
            {
                'first_name': 'TEST 3',
                'last_name': 'LASTNAME',
                'personal_number': '898989-1236',
                'email': 'test3@domain.org'
            },
            {
                'first_name': 'REAL',
                'last_name': 'LASTNAME',
                'personal_number': '898989-1237',
                'email': 'test3@domain.org'
            },
        ])

        # Try to delete old data now, should delete 3 Applicant records
        result = self.env['casemanagement.campaign'].remove_old_data(today=today, domain=['name', 'like', 'TEST '])
        self.assertTrue(result[0] == 3, 'Removed Applicants should be 3')
        self.assertTrue(result[1] == 0, 'Removed Applications should be 0')
        _logger.info('=============================== REMOVE OLD DATA 2 ==============================')

        # Create Applications
        created_applicants_1 = self.env['casemanagement.applicant'].create([
            {
                'first_name': 'TEST 1',
                'last_name': 'LASTNAME',
                'personal_number': '898989-1234',
                'email': 'test1@domain.org'
            },
            {
                'first_name': 'TEST 2',
                'last_name': 'LASTNAME',
                'personal_number': '898989-1235',
                'email': 'test2@domain.org'
            }
        ])
        created_applications = self.env['casemanagement.application'].create([
            {
                'name': 'TEST internship 1',
                'employer_id': created_employers[0].id,
                'applicant_id': created_applicants_1[0].id
            },
            {
                'name': 'TEST internship 2',
                'employer_id': created_employers[1].id,
                'applicant_id': created_applicants_1[1].id
            },
            {
                'name': 'TEST internship 3',
                'employer_id': created_employers[2].id,
                'applicant_id': created_applicants_1[0].id
            },
            {
                'name': 'TEST internship 4',
                'employer_id': created_employers[2].id,
                'applicant_id': created_applicants_1[1].id
            }
        ])
        result = self.env['casemanagement.campaign'].remove_old_data(today=today, domain=['name', 'like', 'TEST '])
        self.assertTrue(result[0] == 2, 'Removed Applicants should be 2')
        self.assertTrue(result[1] == 4, 'Removed Applications should be 4')
        _logger.info('=============================== REMOVE OLD DATA 3 ==============================')

    def test_update_approvals(self):
        """
        Campaign model, update_approvals_for_soon_invalid_employers() method
        """
        # Employers
        created_employers = self.env['casemanagement.employer'].create([
            {
                'name': 'TEST employer 1',
                'org_number': '898989-1234',
                'email': 'test@testemployer1.org'
            },
            {
                'name': 'TEST employer 2',
                'org_number': '898989-1235',
                'email': 'test@testemployer2.org'
            },
            {
                'name': 'TEST employer 3',
                'org_number': '898989-1236',
                'email': 'test@testemployer3.org'
            },
        ])
        # Campaign
        created_campaign = self.env['casemanagement.campaign'].create(
            {
                'date_end': datetime.now().date() + timedelta(days=380),
                'employer_ids': [(6, 0, [e.id for e in created_employers])]
            })

        # Create Unions if no existing
        existing_unions = self.env['casemanagement.union'].search([])
        if not existing_unions:
            existing_unions = self.env['casemanagement.union'].create([
                {
                    'name': 'Akavia',
                    'email': 'noreply@jobtech-akavia.se'
                },
                {
                    'name': 'Jusek',
                    'email': 'noreply@jobtech-jusek.se'
                },
                {
                    'name': 'Sveriges Ingenjörer',
                    'email': 'noreply@jobtech-sako.se'
                },
                {
                    'name': 'Sveriges Arkitekter',
                    'email': 'noreply@jobtech-sveark.se'
                },
                {
                    'name': 'Naturvetarna',
                    'email': 'noreply@jobtech-naturvetarna.se'
                }
            ])

        existing_approvals = self.env['casemanagement.approval'].search([
            ['employer_id', 'in', [e.id for e in created_employers]]
        ])

        # No Approvals yet, so no emails should be sent
        sent_emails = created_campaign.update_approvals_for_soon_invalid_employers()
        self.assertTrue(sent_emails == 0)

        # Create Approval records for all Campaign.employer_ids and all existing Unions
        guid_mapping = created_campaign.create_union_approvals()

        created_approvals = self.env['casemanagement.approval'].search([
            ['employer_id', 'in', [e.id for e in created_employers]]
        ])

        self.assertTrue(len(created_approvals) == len(created_employers) * len(existing_unions))

        # Sign Approvals, now Employers should be approved
        created_approvals.sign_approval('19890101-0001')

        # We only signed approval just now, so no new UnionApproval records should be created and no emails sent
        _logger.info('================== DEBUG TEST 1 ==================')
        sent_emails = created_campaign.update_approvals_for_soon_invalid_employers()
        self.assertTrue(sent_emails == 0)

        # Now we travel forward in time 370 days and check that new emails are sent
        _logger.info('================== DEBUG TEST 2 ==================')
        today = datetime.now() + timedelta(days=370)
        sent_emails = created_campaign.update_approvals_for_soon_invalid_employers(today=today)
        self.assertTrue(sent_emails == len(existing_unions))

        # Subsequent runs should not send more emails
        _logger.info('================== DEBUG TEST 3 ==================')
        sent_emails = created_campaign.update_approvals_for_soon_invalid_employers(today=today)
        self.assertTrue(sent_emails == 0)
