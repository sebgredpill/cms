from odoo.tests import tagged, common

from ..util.solid import *


@tagged('casemanagement')
class TestSolid(common.TransactionCase):

    def test_client_credentials_login(self):
        # First authentication
        session_dict = dict()
        solid_client_credentials_login(session_dict)

        self.assertTrue(session_dict)

        for k in ['solid_client_id', 'solid_client_secret', 'solid_key', 'solid_access_token_response',
                  'solid_access_token', 'solid_token_valid_until']:
            self.assertIn(k, session_dict)

        self.assertIsInstance(session_dict['solid_token_valid_until'], datetime.datetime)
        self.assertTrue(datetime.datetime.now() < session_dict['solid_token_valid_until'])
        self.assertTrue(datetime.datetime.now() <
                        session_dict['solid_token_valid_until'] <
                        datetime.datetime.now() + datetime.timedelta(seconds=600))
        self.assertFalse(session_dict['solid_token_valid_until'] <
                         datetime.datetime.now() + datetime.timedelta(seconds=590))

        # Second authentication, re-use
        old_key = session_dict.get('solid_key')
        old_client_id = session_dict.get('solid_client_id')
        old_client_secret = session_dict.get('solid_client_secret')
        old_token = session_dict.get('solid_access_token')
        old_valid_until = session_dict.get('solid_token_valid_until')

        solid_client_credentials_login(session_dict)

        self.assertTrue(old_key == session_dict.get('solid_key'))
        self.assertTrue(old_client_id == session_dict.get('solid_client_id'))
        self.assertTrue(old_client_secret == session_dict.get('solid_client_secret'))
        self.assertTrue(old_token == session_dict.get('solid_access_token'))
        self.assertTrue(old_valid_until == session_dict.get('solid_token_valid_until'))

        # Simulate invalidation of token, so new one has to be issued using the same client_id and client_secret
        self.assertTrue(valid_access_token_in_session(session_dict))
        session_dict['solid_token_valid_until'] = datetime.datetime.now() - datetime.timedelta(seconds=600)
        self.assertFalse(valid_access_token_in_session(session_dict))

        old_key = session_dict.get('solid_key')
        old_client_id = session_dict.get('solid_client_id')
        old_client_secret = session_dict.get('solid_client_secret')
        old_token = session_dict.get('solid_access_token')
        old_valid_until = session_dict.get('solid_token_valid_until')

        solid_client_credentials_login(session_dict)

        self.assertTrue(old_key == session_dict.get('solid_key'))
        self.assertTrue(old_client_id == session_dict.get('solid_client_id'))
        self.assertTrue(old_client_secret == session_dict.get('solid_client_secret'))
        self.assertFalse(old_token == session_dict.get('solid_access_token'))
        self.assertFalse(old_valid_until == session_dict.get('solid_token_valid_until'))
