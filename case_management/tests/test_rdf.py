import logging

from odoo.tests import tagged, common

from ..util.rdf import collect_rdf_graph_data

_logger = logging.getLogger(__name__)


@tagged('casemanagement')
class TestNodeVC(common.TransactionCase):

    def test_issue_vc(self):
        migration_data = """
        @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
        @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
        @prefix foaf: <http://xmlns.com/foaf/0.1/> .
        
        <#http://localhost:3000/user123>
            a foaf:Person ;
            foaf:personalNumber "880202-4545" ;
            foaf:registrationDate "1989-01-01" ;
            foaf:givenName "Mary" ;
            foaf:familyName "Johnson" ;
            foaf:originCountry "non-eu" ;
            foaf:residentPermitType "put" ;
            foaf:residentPermitTypeGood "True" . 
        """
        tax_data = """
        @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
        @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
        @prefix foaf: <http://xmlns.com/foaf/0.1/> .
        
        <#http://localhost:3000/user123>
            a foaf:Person ;
            foaf:personalNumber "880202-4545" ;
            foaf:name "Mary Johnson" ;
            foaf:employer "Aktiebolag" ; 
            foaf:employerGood "True" ; . 
        """

        d = dict()
        for data in [migration_data, tax_data]:
            collect_rdf_graph_data(d, data)

        self.assertTrue(d)
        self.assertTrue("personal_number" in d)
        self.assertTrue("first_name" in d)
        self.assertTrue("last_name" in d)
        self.assertTrue("name" in d)
        self.assertTrue("employer" in d)
        self.assertTrue("registration_date" in d)
        self.assertTrue("residence_permit_type" in d)

        _logger.info("============================= COLLECTED PYTHON DATA ============================")
        _logger.info(d)
        _logger.info("================================================================================")
