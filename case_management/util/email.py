import logging
import os
import re
import smtplib
import ssl
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from .solid import APP_SERVER_URL

_logger = logging.getLogger(__name__)

SMTP_SERVER = os.environ.get("CMS_EMAIL_SERVER", "email-smtp.eu-north-1.amazonaws.com")
SMTP_PORT = os.environ.get("CMS_EMAIL_PORT", 465)
SENDER_EMAIL = os.environ.get("CMS_EMAIL_SENDER", "noreply@af.se")
SMTP_USERNAME = os.environ.get("CMS_EMAIL_USERNAME", "noreply@af.se")
SMTP_PASSWORD = os.environ.get("CMS_EMAIL_PASSWORD", "pass")
JS_EMAIL = os.environ.get("JS_EMAIL", "internship@jobbspranget.se")

EMAIL_REGEX = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'


def send_email(applicant, email_content, subject=None, attachment=None, cc=None):
    """
    Send email to a given Applicant
    :param applicant: Applicant to send email to
    :type applicant: Applicant | Employer
    :param email_content: Email body
    :type email_content: str
    :param subject: Email subject
    :type subject: str
    :param attachment: Dict with file name and binary content
    :type attachment: dict
    :param cc: CC address(es) to send email copy to
    :type cc: str | list
    """
    if not subject:
        subject = "Internship Application"
    if not valid_email(applicant.email):
        error_message = f"Invalid email address '{applicant.email}' for Applicant '{applicant}'"
        _logger.error(error_message)
        raise ValueError(error_message)

    _logger.info(f"Sending email to '{applicant.email}' with subject '{subject}' and content '{email_content}'")

    try:
        # This one can contain both HTML and plain text messages
        message = MIMEMultipart("alternative")
        message["Subject"] = subject
        message["From"] = SENDER_EMAIL
        message["To"] = applicant.email
        if cc:
            message["Cc"] = cc

        # Create the plain-text and HTML version of your message
        text = f"""\
Hi {applicant.name},

{email_content}


--
Best regards,
The Internship Team"""

        html = f"""\
<html>
<body>
  <p>Hi {applicant.name},</p>
  <br><br>
{email_content}
  <br><br>
  <p>--</p>
  <p>Best regards,</p>
  <p>The Internship Team</p>
</body>
</html>"""

        # Turn these into plain/html MIMEText objects
        plain_part = MIMEText(text, "plain")
        html_part = MIMEText(html, "html")

        # Add HTML/plain-text parts to MIMEMultipart message
        # The email client will try to render the last part first
        message.attach(plain_part)
        message.attach(html_part)

        # Add attachment file (only 1 attachment per email for now)
        if attachment and 'binary' in attachment and 'name' in attachment:
            file_name = attachment.get('name')
            file_part = MIMEApplication(
                attachment.get('binary'),
                Name=file_name
            )
            file_part['Content-Disposition'] = f'attachment; filename="{file_name}"'
            message.attach(file_part)

        _logger.info("==================================================================")
        _logger.info("Trying to send the email below")
        _logger.info(message.as_string())
        _logger.info("==================================================================")

        # Establish secure connection and send email
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(SMTP_SERVER, SMTP_PORT, context=context) as server:
            server.login(SMTP_USERNAME, SMTP_PASSWORD)
            server.sendmail(SENDER_EMAIL, applicant.email, message.as_string())

        return True

    except Exception as e:
        _logger.error(f"Error when sending email to '{applicant.email}': '{e}'")
        return False


def send_email_with_attachment(applicant, post_data, save_as_comment=True, cc=None):
    """
    Send email to given Applicant, extract attachment from POST data and send as attachment
    :param applicant: Applicant to send email to
    :type applicant: Applicant | Employer
    :param post_data: Dict with POST form data
    :type post_data: dict
    :param save_as_comment: By default, save email and attachment as a comment
    :type save_as_comment: bool
    :param cc: CC address(es) to send email copy to
    :type cc: str | list
    """
    file_dict = extract_attachment_data(post_data)
    email_subject = post_data.get('subject')
    email_content = post_data.get('content')
    if save_as_comment:
        applicant.attach_document(file_dict.get('name'),
                                  file_dict.get('binary'),
                                  f'Email sent:\n\n{email_subject}\n\n----------\n\n{email_content}')
    send_email(applicant, email_content, email_subject, file_dict, cc=cc)


def extract_attachment_data(post_data, param_name='attachment'):
    """
    Extract data about attached file
    :param post_data: POST data to extract file attachment data from
    :param param_name: Name of parameter holding attachment
    :return: Dict with attachment data
    :rtype: dict
    """
    # TODO: security, size limit
    file_dict = dict()
    if post_data.get(param_name):
        file_name = post_data.get(param_name).filename
        file = post_data.get(param_name)
        file_dict['name'] = file_name
        file_dict['binary'] = file.read()
        file_dict['comment_text'] = post_data.get('comment_text')

    return file_dict


def valid_email(email_address):
    """
    Checks if email_address is a syntactically valid email address
    :param email_address: Email address string
    :type email_address: str
    :rtype: bool
    """
    return re.fullmatch(EMAIL_REGEX, str(email_address))


def send_emails_to_unions(mapping):
    """
    Sends emails to Unions with unique URLs in order to sign batches of Employers included in Campaign
    :param mapping: Dictionary with 'guid', 'id' and 'union' as keys
    :type mapping: dict
    """
    # TODO: Update email_content below with full information to the Unions
    email_content = """Ett stort antal företag ingår i Jobbsprångets satsning på praktikplatser i Sverige
            för akademiker från länder utanför EU. För att förenkla samrådsförfarandet med berörda fackförbund
            .... mer förklaring här .... ber vi er att signera yttrandet om samråd i vår webbtjänst.
            Där finns även listan med de arbetsgivare som omfattas för nedladdning, samt instruktion för att hantera
            eventuella invändningar mot specifika arbetsgivares deltagande.\n\n
            Till Arbetsförmedlingens webbtjänst: <a href="%s">länk</a>.
            """
    for g in mapping:
        union = g.get('union')
        unique_url = f"{APP_SERVER_URL}casemanagement/union/sign/{g.get('guid')}"
        if union and union.email:
            send_email(union,
                       email_content % unique_url,
                       'JobbSprånget - signera yttrande om arbetsgivare',
                       cc=JS_EMAIL)
