import base64
import datetime
import hashlib
import json
import logging
import os

import jwcrypto.jwk
import jwcrypto.jwt
import requests

from solidclient.solid_client import SolidSession, SolidAPIWrapper

from .vc import NodeVC
from odoo import http

from odoo.http import request

SOLID_SERVER_URL = os.environ.get("SOLID_SERVER_URL", "http://localhost:3000")
SECRET_SOLID_SERVER_EMAIL = os.environ.get("SECRET_SOLID_SERVER_EMAIL", "source@example.com")
SECRET_SOLID_SERVER_PW = os.environ.get("SECRET_SOLID_SERVER_PW", "source")
APP_SERVER_URL = os.environ.get("APP_SERVER_URL", "http://localhost:8069/")
APP_SECRET_KEY_PATH = os.environ.get("APP_SECRET_KEY_PATH", "/mnt/extra-addons/case_management/secret")
NODE_VC_SCRIPT_PATH = os.environ.get("NODE_VC_SCRIPT_PATH", '/var/oak/solid-client-node-experimentation/issue-vc.js')

_logger = logging.getLogger(__name__)


def subscribe_resource(subscription_resource, session):
    """
    Subscribe Solid Pod server resource
    :param subscription_resource: Solid Pod server URI to create subscription to
    :type subscription_resource: str
    :param session: Session to store state in
    :type session: dict | Session
    """
    solid_session = SolidSession(state_storage=session)

    if valid_access_token_in_session(session):
        solid_session.keypair = session['solid_key']
        solid_session.access_token = session['solid_access_token']
        _logger.info("SESSION solid key '{}'".format(session['solid_key']))
        _logger.info("SESSION solid access_token '{}'".format(session['solid_access_token']))
    elif valid_access_token_in_file():
        update_session_with_token_from_file(session)
        solid_session.keypair = session['solid_key']
        solid_session.access_token = session['solid_access_token']
        _logger.info("FILE solid key '{}'".format(session['solid_key']))
        _logger.info("FILE solid access_token '{}'".format(session['solid_access_token']))
    else:
        # Not authenticated, start the process and redirect back to this endpoint
        session['solid_subscription_resource_uri'] = subscription_resource

        # Update session even if token is invalid, other values like key, client_id and client_secret are re-used
        update_session_with_token_from_file(session)

        solid_client_credentials_login(session)
        solid_session.keypair = session['solid_key']
        solid_session.access_token = session['solid_access_token']

    # Subscribe to a resource on Solid server
    if session.get('solid_subscription_resource_uri'):
        subscription_resource = session.pop('solid_subscription_resource_uri')

    if subscription_resource:
        _logger.info("==============================================")
        _logger.info(f"Subscribing to notifications on resource '{subscription_resource}'")

        # Negotiate for WebHook protocol
        gateway_data = {
            "@context": ["https://www.w3.org/ns/solid/notification/v1"],
            "type": ["WebHookSubscription2021"],
            "features": ["state", "webhook-auth"],
        }
        try:
            # Usually /gateway
            notification_endpoint = solid_session.solid_metadata.get('notification_endpoint')
            gateway_response = solid_session.post(notification_endpoint,
                                                  json.dumps(gateway_data)).json()
            _logger.info("====================== Gateway response ========================")
            _logger.info(gateway_response)

            # Usually /subscription
            subscription_endpoint = gateway_response.get('endpoint')

            # Request new WebHook subscription
            subscription_data = {
                "@context": ["https://www.w3.org/ns/solid/notification/v1"],
                "type": "WebHookSubscription2021",
                "topic": subscription_resource,
                "target": f"{APP_SERVER_URL}casemanagement/solid-notification-webhook",
            }
            # Check if not subscribed yet - only allow a single subscription with unique digest value
            # Prepare data for Subscription object
            sub_data = dict(digest=get_digest(subscription_data))
            sub_data.update(subscription_data)
            found_subscription = http.request.env['casemanagement.subscription'] \
                .search([['digest', '=', sub_data.get('digest')]], count=True)
            if not found_subscription:
                subscription_response = solid_session.post(subscription_endpoint,
                                                           json.dumps(subscription_data))
                _logger.info("======================== Subscription response ========================")
                _logger.info(subscription_response.raw_text)

                if subscription_response.status_code != 200 and "No WebId present" in subscription_response.raw_text:
                    # Key and access_token most likely expired, start auth and redirect back to this endpoint
                    session['solid_subscription_resource_uri'] = subscription_resource
                    # Forget expired key and access_token
                    session.pop('solid_key')
                    session.pop('solid_access_token')

                    # Perform login to Solid Pod server, extract and save key and access token
                    solid_client_credentials_login(session)
                    return subscribe_resource(subscription_resource, session)

                data = {
                    'digest': sub_data.get('digest'),
                    'type': sub_data.get('type'),
                    'subscription_target': sub_data.get('topic'),
                    'unsubscribe_endpoint': subscription_response.json().get('unsubscribe_endpoint'),
                    'raw_json': json.dumps(subscription_response.json()),
                }
                http.request.env['casemanagement.subscription'].create(data)

                modal_message = "Subscribed to resource '{}'; response: '{}'".format(subscription_resource,
                                                                                     subscription_response.raw_text)
            else:
                modal_message = "Tried to subscribe to resource '{}', but Subscription " \
                                "with identical digest already exists".format(subscription_resource)
                _logger.info("==============================================")
                _logger.info(modal_message)
        except requests.exceptions.MissingSchema as e:
            modal_message = "Tried to subscribe to resource '{}', but some endpoint is not available on Solid Pod " \
                            "server. Error: \"{}\"".format(subscription_resource, e)
            _logger.error("==============================================")
            _logger.error(modal_message)
        except Exception as e:
            modal_message = "Tried to subscribe to resource '{}', but unexpected error occurred: " \
                            "'{}'".format(subscription_resource, e)
            _logger.error("==============================================")
            _logger.error(modal_message)
    else:
        modal_message = "No notification subscription resource found"
        _logger.info(modal_message)

    return modal_message


def solid_login_workaround(auth_url):
    """
    Performs Solid Pod server login without user interaction given username and password are available
    Temporary workaround until a different authentication flow is possible
    :param auth_url: URL to visit using browser
    :type auth_url: str
    """
    # Visit authentication URL to get cookies
    response = requests.get(auth_url, allow_redirects=False)
    cookies = response.cookies

    _logger.info("==================== GET auth_url response ==========================")
    _logger.info(response)
    _logger.info("==================== GET auth_url cookies ==========================")
    _logger.info(cookies)
    _logger.info("==================== GET auth_url headers ==========================")
    _logger.info(response.headers)
    _logger.info("==================== GET auth_url text ==========================")
    _logger.info(response.text)

    # Not sure if this /idp/ URL is available in provider_info?
    # idp_url = solid_session.provider_info
    # _logger.info("==================== SolidSession IDP provider info ==========================")
    # _logger.info(idp_url)

    # POST login credentials with cookies from the above call
    post_response = requests.post(SOLID_SERVER_URL + "/idp/",
                                  data=dict(email=SECRET_SOLID_SERVER_EMAIL,
                                            password=SECRET_SOLID_SERVER_PW,
                                            remember="yes"),
                                  cookies=cookies,
                                  allow_redirects=False)
    location = post_response.headers.get("Location")

    _logger.info("==================== POST response text ==========================")
    _logger.info(post_response.text)
    _logger.info("==================== POST response headers ==========================")
    _logger.info(post_response.headers)
    _logger.info(location)

    # Complete authentication after successful POST to /idp/
    location_response = requests.get(location,
                                     cookies=cookies,
                                     allow_redirects=False)
    redirect_location = location_response.headers.get("Location")

    _logger.info("==================== GET location response ==========================")
    _logger.info(location_response)
    _logger.info("==================== GET location headers ==========================")
    _logger.info(location_response.headers)
    _logger.info("==================== REDIRECT URL ==========================")
    _logger.info(redirect_location)

    # This final redirect triggers retrieval of access token from Solid Pod server
    return request.redirect(redirect_location)


def solid_client_credentials_login(session_storage, overwrite_existing_token=False, persist=True):
    """
    Performs Solid Pod server login using client credentials flow
    Access token to be used for authenticated requests to Solid Pod server is written to session_storage
    :param session_storage: Session to store token and other data to
    :type session_storage: dict
    :param overwrite_existing_token: Overwrites existing token if True
    :type overwrite_existing_token: bool
    :param persist: Save newly issued token and other related data in a file
    :type persist: bool
    """
    if not valid_access_token_in_session(session_storage) or overwrite_existing_token:
        # Generate/retrieve a key-pair
        keypair = session_storage.get('solid_key') or jwcrypto.jwk.JWK.generate(kty='EC', crv='P-256')
        key = jwcrypto.jwk.JWK.from_json(keypair) if type(keypair) == str else keypair

        solid_session = SolidSession(key, state_storage=session_storage)

        if 'solid_client_id' in session_storage and 'solid_client_secret' in session_storage:
            cred_client_id = session_storage.get('solid_client_id')
            cred_client_secret = session_storage.get('solid_client_secret')
        else:
            credentials_response = requests.post(solid_session.idp_info['credentials'],
                                                 json=dict(email=SECRET_SOLID_SERVER_EMAIL,
                                                           password=SECRET_SOLID_SERVER_PW,
                                                           name='source_client_credentials_login'))
            credentials_response_json = credentials_response.json()

            if credentials_response.status_code != 200:
                _logger.error('========================= CLIENT CREDENTIALS LOGIN ERROR =========================')
                _logger.error(json.dumps(credentials_response_json, indent=4))
                credentials_response.raise_for_status()

            cred_client_id = credentials_response_json.get('id')
            cred_client_secret = credentials_response_json.get('secret')
            _logger.info('============================ CLIENT CREDENTIALS LOGIN 1 ============================')
            _logger.info(json.dumps(credentials_response_json, indent=4))
            _logger.info(credentials_response.status_code, credentials_response.text)

        # Retrieve Access Token
        basic_secret = base64.b64encode((cred_client_id + ':' + cred_client_secret).encode('utf-8'))
        token_response = requests.post(url=solid_session.provider_info['token_endpoint'],
                                       data={
                                           "grant_type": "client_credentials",
                                           "scope": "webid"
                                       },
                                       headers={
                                           'Authorization': 'Basic ' + basic_secret.decode('utf-8'),
                                           'DPoP': solid_session.make_token(key,
                                                                            solid_session.provider_info[
                                                                                'token_endpoint'],
                                                                            'POST')
                                       },
                                       allow_redirects=True)
        token_response_json = token_response.json()

        if token_response.status_code != 200:
            _logger.error('========================= CLIENT CREDENTIALS LOGIN ERROR =========================')
            _logger.error(json.dumps(token_response_json, indent=4))
            token_response.raise_for_status()

        # Save in session for future use
        token_expires_datetime = datetime.datetime.now() + datetime.timedelta(
            seconds=int(token_response_json['expires_in']) - 5)
        session_storage['solid_client_id'] = cred_client_id
        session_storage['solid_client_secret'] = cred_client_secret
        session_storage['solid_key'] = key.export()
        session_storage['solid_access_token_response'] = token_response_json
        session_storage['solid_access_token'] = token_response_json['access_token']
        session_storage['solid_token_valid_until'] = token_expires_datetime

        # Save to file for re-use
        if persist:
            write_key_and_access_token_to_file(session_storage['solid_key'],
                                               session_storage['solid_access_token'],
                                               session_storage['solid_access_token_response'],
                                               session_storage['solid_client_id'],
                                               session_storage['solid_client_secret'])

        _logger.info('============================ CLIENT CREDENTIALS LOGIN 2 ============================')
        _logger.info(json.dumps(token_response_json, indent=4))


def get_digest(subscription_data):
    """
    Computes SHA256 digest of the given dict represented as string
    :param subscription_data: Subscription data sent to Solid Pod server in order to subscribe to notifications
    :type subscription_data: dict
    :rtype: str
    """
    d = hashlib.sha256()
    d.update(json.dumps(subscription_data).encode())

    return d.hexdigest()


def put_response_to_solid(personal_number, solid_guid):
    """
    PUT response, response link to correct place in Solid Pod server, DELETE consent-link
    :param personal_number: Personal number of Applicant to issue VC for
    :type personal_number: str
    :param solid_guid: GUID string used to identify Solid resources belonging to a particular response
    :type solid_guid: str
    """
    try:
        if not solid_guid or len(solid_guid) < 10:
            raise ValueError("Wrong value for parameter solid_guid, not GUID")

        # Issue VC and PUT it in Solid Pod
        issuer = NodeVC(NODE_VC_SCRIPT_PATH)
        vc = issuer.issue_vc(personal_number, solid_guid)
        jsonld = vc.get('jsonld')

        _logger.info("============================ DEMO 3 VC ============================")
        _logger.info(jsonld)

        key, access_token, raw_token, client_id, client_secret, valid_until = get_key_and_access_token_from_file()
        solid_session = SolidAPIWrapper(client_id=client_id,
                                        client_secret=client_secret,
                                        access_token=raw_token,
                                        keypair=key,
                                        logger=_logger)

        # PUT http://localhost:3000/user/oak/responses/response-GUID
        response_url = f"{SOLID_SERVER_URL}/user/oak/responses/response-{solid_guid}"
        resp = solid_session.put_file(response_url,
                                      json.dumps(jsonld, indent=2),
                                      'application/ld+json')

        _logger.info("============================ DEMO 4 ============================")
        _logger.info(json.dumps(jsonld, indent=2))
        _logger.info("========================================================")
        _logger.info(resp.status_code)
        _logger.info(resp.text)

        # PUT http://localhost:3000/user/oak/inbox/response-link-GUID
        # Content: `<> <https://oak.se/dataResponseUrl> <${url}>.`
        response_link = f"<> <https://oak.se/dataResponseUrl> <{response_url}>."
        response_link_url = f"{SOLID_SERVER_URL}/user/oak/inbox/response-link-{solid_guid}"
        resp_link = solid_session.put_file(response_link_url, response_link, 'text/turtle')

        _logger.info("============================ DEMO 5 ============================")
        _logger.info(resp_link)
        _logger.info(resp_link.text)

        # DELETE: http://localhost:3000/source/oak/inbox/consent-link-GUID
        delete_url = f"{SOLID_SERVER_URL}/source/oak/inbox/consent-link-{solid_guid}"
        resp_delete = solid_session.delete(delete_url)

        _logger.info("============================ DEMO 6 ============================")
        _logger.info(resp_delete)
        _logger.info(resp_delete.text)
    except Exception as e:
        _logger.error("============================ DEMO ERROR ============================")
        _logger.error(e)


def get_key_and_access_token_from_file(username="source"):
    """
    Retrieves key and access token to be used when communicating with Solid server
    :param username: Username to get data for
    :type username: str
    :rtype: (str, str, dict, str, str, datetime.datetime)
    :return: Key and access token for given user
    """
    secret_key = f"{APP_SECRET_KEY_PATH}/{username}_user.key"
    try:
        with open(secret_key, "r") as sink_user_keys:
            json_loaded = json.loads(sink_user_keys.read())
            keypair = jwcrypto.jwk.JWK.from_json(json_loaded["key"])
            access_token = json_loaded["access_token"]
            raw_token = json_loaded["raw_token"]
            client_id = json_loaded["client_id"]
            client_secret = json_loaded["client_secret"]
            valid_until = datetime.datetime.fromtimestamp(int(json_loaded["valid_until"]))
    except Exception as e:
        _logger.error(f"Error when loading key for user '{username}': '{e}'")
        keypair = "NO_KEY_FOUND"
        access_token = "NO_ACCESS_TOKEN_FOUND"
        raw_token = "NO_RAW_TOKEN_FOUND"
        client_id = "NO_CLIENT_ID_FOUND"
        client_secret = "NO_CLIENT_SECRET_FOUND"
        valid_until = "NO_VALID_UNTIL_FOUND"

    return keypair, access_token, raw_token, client_id, client_secret, valid_until


def write_key_and_access_token_to_file(key, access_token, raw_token, client_id, client_secret):
    """
    Writes key and access token to a file for given username
    :param key: JWK key to write
    :type key: str
    :param access_token: JWT access token to write
    :type access_token: str
    :param raw_token: Full response object
    :type raw_token: dict
    :param client_id: Client ID
    :type client_id: str
    :param client_secret: Client secret
    :type client_secret: str
    :rtype: bool
    """
    try:

        decoded_access_token = jwcrypto.jwt.JWT()
        decoded_access_token.deserialize(access_token)

        web_id = json.loads(decoded_access_token.token.objects['payload'])['webid']
        username = web_id.split("/")[3]

        token_expires_datetime = datetime.datetime.now() + datetime.timedelta(seconds=int(raw_token['expires_in']) - 5)
        out_to_file = dict(key=key,
                           access_token=access_token,
                           raw_token=raw_token,
                           client_id=client_id,
                           client_secret=client_secret,
                           valid_until=int(token_expires_datetime.timestamp()))

        filename = f"{APP_SECRET_KEY_PATH}/{username}_user.key"
        with open(filename, 'w+') as output_file:
            _logger.info(f"Dumping JWT and access token to file '{filename}'")
            output_file.write(json.dumps(out_to_file))
    except Exception as e:
        _logger.error(f"Error when saving key '{key}' and access_token '{access_token}': '{e}'")
        return False

    return True


def valid_access_token_in_session(session):
    """
    Checks if Solid access token is available in session, checks if it expired (if exists)
    :param session: Session object that holds Solid access token and other related data
    :type session: dict
    """
    valid_until = session.get('solid_token_valid_until')
    return all([
        session.get('solid_key'),
        session.get('solid_access_token'),
        session.get('solid_access_token_response'),
        session.get('solid_client_id'),
        session.get('solid_client_secret'),
        valid_until and valid_until > datetime.datetime.now()])


def valid_access_token_in_file(username="source"):
    """
    Checks if file containing Solid access token exists and whether its access token is still valid
    :param username: Username to get data for
    :type username: str
    :rtype: bool
    """
    key, access_token, raw_token, client_id, client_secret, valid_until = get_key_and_access_token_from_file(username)

    # All strings means no valid token found in file
    if all([type(v) == str for v in [key, access_token, raw_token, client_id, client_secret, valid_until]]):
        return False

    return all([key, access_token, raw_token, client_id, client_secret,
                valid_until and valid_until > datetime.datetime.now()])


def update_session_with_token_from_file(session, username="source"):
    """
    Updates session with Solid access token and other related data d
    :param session: Session object to update with data
    :type session: dict
    :param username: Username to get data for
    :type username: str
    """
    key, access_token, raw_token, client_id, client_secret, valid_until = get_key_and_access_token_from_file(username)
    if all([type(v) == str for v in [key, access_token, raw_token, client_id, client_secret, valid_until]]):
        return False
    else:
        session['solid_key'] = key
        session['solid_access_token'] = access_token
        session['solid_access_token_response'] = raw_token
        session['solid_client_id'] = client_id
        session['solid_client_secret'] = client_secret
        session['solid_token_valid_until'] = valid_until
        return True


def get_authenticated_solid_session(session):
    """
    Checks if valid token already exists in session or in file
    Issues a new token if it does not exist
    Returns SolidSession
    :param session: Session object, dict-like
    :type session: dict
    :rtype: SolidSession
    :returns: Authenticated SolidSession object to be used for simple interaction with Solid Pod server
    """
    if valid_access_token_in_session(session):
        _logger.info("SESSION solid key '{}'".format(session['solid_key']))
        _logger.info("SESSION solid access_token '{}'".format(session['solid_access_token']))
    elif valid_access_token_in_file():
        update_session_with_token_from_file(session)
        _logger.info("FILE solid key '{}'".format(session['solid_key']))
        _logger.info("FILE solid access_token '{}'".format(session['solid_access_token']))
    else:
        # Update session even if token is invalid, other values like key, client_id and client_secret are re-used
        update_session_with_token_from_file(session)
        solid_client_credentials_login(session)

    solid_session = SolidSession(keypair=session.get('solid_key'),
                                 access_token=session.get('solid_access_token'),
                                 state_storage=session)

    return solid_session
