import json
import logging
import os
import uuid

import requests
from requests.exceptions import SSLError

_logger = logging.getLogger(__name__)

MOCK = bool(os.environ.get("API_MV_MOCK"))


def migration_details_by_seeker_id(job_seeker_id, mock=MOCK):
    """
    Contacts API and fetches information for a given job seeker ID
    :param job_seeker_id: AFs internal job seeker ID
    :type job_seeker_id: int
    :param mock: If True or dict, does not perform an API call but return mock data
    :type mock: bool | dict
    :rtype: dict
    """
    assert type(job_seeker_id) == int

    if not mock:
        endpoint = os.environ.get("API_MV_URL", "https://api-url/{}/migration")
        system_id = os.environ.get("API_MV_SYSTEMID", "testsystem")
        end_user_id = os.environ.get("API_MV_ENDUSERID", "testuser")
        environment = os.environ.get("API_MV_ENVIRONMENT", "env")
        client_id = os.environ.get("API_MV_CLIENT_ID", "client_id")
        client_secret = os.environ.get("API_MV_CLIENT_SECRET", "client_secret")

        url = "%s?client_id=%s&client_secret=%s" % (endpoint.format(job_seeker_id), client_id, client_secret)
        headers = {
            "AF-TrackingId": str(uuid.uuid4()),
            "AF-SystemId": system_id,
            "AF-EndUserId": end_user_id,
            "AF-Environment": environment
        }
        payload = {}

        try:
            # TODO: Remove verify=False below and import the cert in production?
            response = requests.request("GET", url, headers=headers, data=payload, verify=False).json()
        except SSLError as e:
            _logger.error("SSLError '%s' when contacting API at '%s'" % (e, url))
            response = dict(status_code=500, reason=str(e))
        except (StopIteration, json.decoder.JSONDecodeError) as e:
            _logger.error("JSONDecodeError '%s' when contacting API at '%s'" % (e, url))
            response = dict(status_code=500, reason=str(e))
        except Exception as e:
            _logger.error("Error '%s' when contacting API at '%s'" % (e, url))
            response = dict(status_code=500, reason=str(e))
    else:
        response = dict(
            arbetssokande=dict(
                sokandeId=job_seeker_id,
                personnummer="192001051234",
                fornamn="Hej",
                efternamn="Testare",
                fodelsedatum="19200105"),
            migration=dict(
                dossienummer=1234567,
                nyanland=True,
                decision_code="K5",
                senasteUppehallstillstandTypsKod="PUT",
                senasteUppehallstillstandSlutdatum="2021-12-31",
            )
        ) if not type(mock) == dict else mock

    return response
