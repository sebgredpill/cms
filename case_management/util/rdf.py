import logging

from rdflib import Graph

_logger = logging.getLogger(__name__)


def collect_rdf_graph_data(collected_data, input_data):
    """
    Parse input data as "turtle" format into Graph, update collected_data dict with data of interest
    :param collected_data: Dict to save extracted data to
    :type collected_data: dict
    :param input_data: String in "turtle" format to parse and extract data from
    :type input_data: str
    """
    try:
        rdf_graph = Graph().parse(data=input_data, format="turtle")

        # Could use a data model map of some kind to know what we should parse
        for s, p, o in rdf_graph:
            v = o.toPython()
            if "personalNumber" in p:
                collected_data["personal_number"] = v

            elif "residentPermitTypeGood" in p:
                collected_data["residence_permit_type_good"] = v

            elif "name" in p:
                collected_data["name"] = v

            elif "familyName" in p:
                collected_data["last_name"] = v

            elif "givenName" in p:
                collected_data["first_name"] = v

            elif "residentPermitType" in p:
                collected_data["residence_permit_type"] = v

            elif "registrationDate" in p:
                collected_data["registration_date"] = v

            elif "employer" in p and "Good" not in p:
                collected_data["employer"] = v

            elif "employerGood" in p:
                collected_data["employer_good"] = v

            elif "originCountry" in p:
                collected_data["origin_country"] = v

            elif "dataRequest" in p:
                collected_data["data_request"] = v

            elif "subjectId" in p:
                collected_data["personal_number"] = v

            elif "dataResponseUrl" in p:
                collected_data["data_response_url"] = v

            elif "identifier" in p:
                collected_data["identifier"] = v

            _logger.info(f"collect_rdf_graph_data '{s}' '{p}' '{v}'")

    except Exception as e:
        _logger.error(f"Error when collecting data from Turtle input: '{e}'")
