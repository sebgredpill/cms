# -*- coding: utf-8 -*-
import base64
import io
import mimetypes
import os
import re
import uuid

from datetime import datetime, timedelta
import logging
import json

import jwcrypto
import requests as requests

from odoo.exceptions import ValidationError
from odoo.tools import pycompat
from odoo.tools.mimetypes import guess_mimetype
from ..util.api import migration_details_by_seeker_id
from ..util.email import valid_email, send_emails_to_unions
from odoo import models, fields, api, _

from solidclient.utils.utils import make_random_string

_logger = logging.getLogger(__name__)

SOLID_SERVER_URL = os.environ.get("SOLID_SERVER_URL", "http://localhost:3000")


class AttachmentHelper:
    def get_records_to_update(self, *args):
        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env[self._name].browse(request_ids) if not len(self) else self

        return records_to_update

    def attach_document(self, file_name, binary_data, comment_text, *args):
        """
        Attach document with comment to given object
        :param file_name: Attachment name
        :type file_name: str
        :param binary_data: Document binary data
        :type binary_data: bytearray
        :param comment_text: Comment text
        :type comment_text: str
        """
        assert any([file_name, comment_text]), 'Either file_name or comment_text has to be filled'

        records_to_update = self.get_records_to_update(*args)

        for record in records_to_update:
            values = {
                'name': file_name,
                'base64_data': base64.b64encode(binary_data or b'')
            }

            attachment = self.env['casemanagement.attachment'].create(values)
            created_comment = self.env['casemanagement.comment'].create({'comment_text': comment_text,
                                                                         'attachment_ids': [(4, attachment.id)]})
            record.update({
                'comment_ids': [(4, created_comment.id)]
            })

        return records_to_update


class Applicant(models.Model, AttachmentHelper):
    _name = 'casemanagement.applicant'
    _description = 'Applicant applying for an internship'
    _order = "id desc"
    _sql_constraints = [
        ('personal_num_uniq', 'UNIQUE (personal_number)',
         'You can not have two applicants with the same personal number!')
    ]
    _translate = True

    personal_number = fields.Char(
        help='Swedish personal number',
        readonly=True)

    job_seeker_id = fields.Integer(
        help='AF job seeker ID',
        readonly=True)

    first_name = fields.Char(
        help='Applicant first name',
        readonly=True)

    last_name = fields.Char(
        help='Applicant last name',
        readonly=True)

    name = fields.Char(compute="_render_name",
                       help='Applicant full name',
                       readonly=True,
                       store=True)

    phone = fields.Char(
        help='Phone number'
    )

    email = fields.Char(
        help='Email address'
    )

    registration_date = fields.Date(
        help='Date registered with AF as looking for job',
        readonly=True)

    residence_permit_expires_date = fields.Date(
        help='Date when the latest residence permit expires',
        readonly=True)

    classification_code_id = fields.Many2one('casemanagement.code', 'Classification Code')

    residence_permit_type = fields.Selection(
        selection=[
            ('put', 'Permanent Residence Permit'),
            ('t-uat', 'Temporary Residence and Work Permit'),
            ('t-ut', 'Temporary Residence Permit'),
            ('ut', 'Residence Permit'),
            ('uk', 'Residence Card'),
            ('--', '--'),
        ],
        default='--',
        help='PUT/T-UAT/T-UT/UT/UK - residence permit type',
        readonly=True)
    residence_permit_type_good = fields.Boolean(
        compute='_suggest_residence_type_good',
        help='Residence permit eligible',
        readonly=True
    )

    origin_country = fields.Selection(
        selection=[
            ('sweden', 'Sweden'),
            ('scandinavia', 'Scandinavia'),
            ('non-eu', 'non-EU'),
            ('eu', 'EU'),
            ('--', '--'),
        ],
        default='--',
        help='Sweden/Scandinavia/non-EU/EU/-- - country of origin')

    auto_suggestion_approval = fields.Selection(
        compute='_suggest_approval',
        selection=[
            ('eligible', 'Eligible'),
            ('examine', 'Examine'),
        ],
        help='Eligible/Examine - automatically generated value based on Applicant properties',
        default='examine',
        readonly=True)

    approval = fields.Selection(
        selection=[
            ('eligible', 'Eligible'),
            ('examine', 'Examine'),
            ('--', '--'),
        ],
        default='--',
        help='Eligible/Examine/-- - decided by case worker')

    solid_guid = fields.Char(
        help='GUID used to identify Solid resources belonging to Applicant',
        readonly=True)

    application_ids = fields.One2many('casemanagement.application', 'applicant_id', string='Applications')

    comment_ids = fields.One2many('casemanagement.comment', 'applicant_id', string='Comments')

    @api.depends('first_name', 'last_name')
    def _render_name(self):
        """
        Renders Applicant full name
        """
        for record in self:
            record.name = f"{record.first_name} {record.last_name}"

    @api.depends('name', 'phone', 'email', 'personal_number', 'classification_code_id', 'residence_permit_type_good',
                 'origin_country', 'registration_date')
    def _suggest_approval(self):
        """
        Computes automatic approval suggestion based on property values supplied in @api.depends decorator
        """
        for record in self:
            satisfied = all(
                [record.name, record.phone, record.email, record.personal_number, record.residence_permit_type_good,
                 record.registration_date, record.origin_country == 'non-eu'])

            if record.classification_code_id:
                code_eligible = record.classification_code_id.eligible == 'true'
            else:
                code_eligible = False

            record.auto_suggestion_approval = 'eligible' if satisfied and code_eligible else 'examine'

    @api.depends('residence_permit_type')
    def _suggest_residence_type_good(self):
        """
        Determines if residence_permit_type is approved
        """
        for record in self:
            record.residence_permit_type_good = record.residence_permit_type in ['put', 't-uat']

    @api.constrains('email')
    def _check_email(self):
        for record in self:
            if not valid_email(record.email):
                raise ValidationError("Field 'email' has to be a valid email address")

    @api.model
    def fetch_migration_data(self, *args):
        """
        Fetches migration data from API for recordset, active_ids or all
        """
        # The first argument is populated when button clicked in native odoo view using button on a record
        request_ids = args[0] if args else []

        # self._context['active_ids'] is sometimes not populated when args[0] is there, therefore we prefer args[0]
        if not request_ids and self._context and 'active_ids' in self._context:
            request_ids = self._context['active_ids']

        # Filter out records without job_seeker_id
        domain = [('job_seeker_id', '!=', False)]
        if request_ids:
            domain.append(('id', 'in', request_ids))

        # if this is a non-empty record set, we perform update on its elements, otherwise
        records_to_update = self.env['casemanagement.applicant'].search(domain) if not len(self) else self

        for record in records_to_update:
            if record.job_seeker_id:
                _logger.info("Getting Migration Data via API for %s and job_seeker_id %s" % (record,
                                                                                             record.job_seeker_id))
                migration_details = migration_details_by_seeker_id(record.job_seeker_id)
                if 'migration' in migration_details:
                    # TODO: 'decision_code' is not available via API yet, adjust accordingly when becomes available
                    if 'decision_code' in migration_details['migration']:
                        existing_code = self.env['casemanagement.code'].search([
                            ['name', '=', str(migration_details['migration']['decision_code']).upper()]
                        ])
                        if existing_code:
                            record.classification_code_id = existing_code[0].id
                    if 'senasteUppehallstillstandSlutdatum' in migration_details['migration']:
                        record.residence_permit_expires_date = \
                            migration_details['migration']['senasteUppehallstillstandSlutdatum']
            else:
                _logger.info("Trying to fetch Migration Data via API for %s who does not have job_seeker_id" % record)

        if len(records_to_update) == 1:
            # If only 1 record updated, show form view of that record
            return {
                "type": "ir.actions.act_window",
                "res_model": "casemanagement.applicant",
                "res_id": records_to_update[0].id,
                "views": [[False, "form"]],
            }
        elif len(records_to_update) == 0:
            return {
                # "type": "ir.actions.act_window",
                # "res_model": "casemanagement.applicant",
                # "views": [[False, "tree"]],
                "view_id": self.env.ref('case_management.list_applicants_tree').id,
            }
        else:
            # Show tree view of updated records
            return {
                "name": _("Applicants"),
                "type": "ir.actions.act_window",
                "res_model": "casemanagement.applicant",
                "views": [[False, "tree"], [False, "form"]],
                "view_id": self.env.ref('case_management.list_applicants_tree').id,
                "domain": [["id", "in", [r.id for r in records_to_update]]],
            }


class ExtraApiData(models.Model):
    _name = 'casemanagement.extraapidata'
    _description = 'Extra data for case management'

    testing = fields.Char()

    def make_token_for(self, keypair, uri, method):
        print(keypair)
        jwt = jwcrypto.jwt.JWT(header={
            "typ":
                "dpop+jwt",
            "alg":
                "ES256",
            "jwk":
                keypair.export(private_key=False, as_dict=True)
        },
            claims={
                "jti": make_random_string(),
                "htm": method,
                "htu": uri,
                "iat": int(datetime.now().timestamp())
            })
        jwt.make_signed_token(keypair)
        return jwt.serialize()

    def make_headers(self, url, method):
        with open('sensitivedata.dat', 'r') as stored_keys:
            json_loaded = json.loads(stored_keys.read())
            keypair = jwcrypto.jwk.JWK.from_json(json_loaded["key"])
            access_token = json_loaded["access_token"]
            headers = {
                "Authorization": ("DPoP " + access_token),
                "DPoP": self.make_token_for(keypair, url, method)
            }
            return headers

    def construct_access_headers(self, url, method):
        # We need a 'key' and an 'access_token'
        # and construct headers like this:

        with open('sensitivedata.dat', 'r') as stored_keys:
            json_loaded = json.loads(stored_keys.read())
            keypair = jwcrypto.jwk.JWK.from_json(json_loaded["key"])
            access_token = json_loaded["access_token"]
            headers = {
                "Authorization": ("DPoP " + access_token),
                "DPoP": self.make_token_for(keypair, url, method)
            }

        decoded_access_token = jwcrypto.jwt.JWT(jwt=access_token)

        web_id = json.loads(
            decoded_access_token.token.objects["payload"]
        )["sub"]

        return headers

    def send_to_log(self, data):
        url = "http://localhost:5555/log"

        req = requests.post(url, json=data)

        return req.status_code == 200

    def fetch_some_data(self, cr, uid, context=None):
        url = f"{SOLID_SERVER_URL}/source-test-se/profile/"
        _logger.info("fetch_some_data() URL: '{}'".format(url))
        self.send_to_log({"status": "RUNNING"})

        headers = self.construct_access_headers(url, 'GET')
        _logger.info("fetch_some_data() Headers: '{}'".format(url))

        resp = requests.get(url, headers)

        _logger.info("fetch_some_data() Response status code: {}".format(resp.status_code))
        _logger.info("fetch_some_data() Response body: {}".format(resp.text))
        _logger.info("fetch_some_data() Fetching some data finished")


class Employer(models.Model, AttachmentHelper):
    _name = 'casemanagement.employer'
    _description = 'An employer offering internship'
    _order = "id desc"
    _sql_constraints = [
        # ('name_uniq', 'UNIQUE (name)',
        #  'You can not have two employers with the same name!'),
        ('org_number_uniq', 'UNIQUE (org_number)',
         'You can not have two employers with the same organization number!'),
    ]
    _translate = True

    name = fields.Char(help='Employer name', required=True, readonly=True)
    org_number = fields.Char(help='Organization number', required=True, readonly=True)
    bar_number = fields.Char(help='Case number from BÄR', readonly=True)
    email = fields.Char(help='Email address to contact employer', required=True, readonly=True)

    no_payment_problem = fields.Boolean(help='"checked" - no payment problems, "unchecked" - has payment problems',
                                        readonly=True)
    no_tax_debt = fields.Boolean(help='"checked" - no debt, "unchecked" - has debt', readonly=True)
    no_bankruptcy = fields.Boolean(help='"checked" - not bankrupt, "unchecked" - bankrupt', readonly=True)

    union_approved = fields.Selection(
        selection=[
            ('ok', 'OK'),
            ('waiting', 'Waiting'),
            ('--', '--'),
        ],
        default='--',
        compute="_compute_union_approved",
        help='"OK" - approved by union, "Waiting" - request of signature sent to unions, '
             '"--" - nothing sent to unions, or something is wrong',
        readonly=True)

    union_approval_expiration_date = fields.Datetime(
        compute="_compute_union_approval_expiration_date",
        help='Datetime after which Employer approval by Union is no longer valid',
        readonly=True)

    union_approval_latest_date = fields.Datetime(
        compute="_compute_union_approval_latest_date",
        help='Datetime when latest Approval record was created',
        readonly=True)

    auto_suggestion_approval = fields.Selection(
        compute="_suggest_approval",
        selection=[
            ('approved', 'Approved'),
            ('examine', 'Examine'),
            ('unprocessed', 'Unprocessed'),
        ],
        default='unprocessed',
        help='Approved/Examine/Unprocessed - automatically generated value based on Employer property values',
        readonly=True)

    approval = fields.Selection(
        selection=[
            ('approved', 'Approved'),
            ('examine', 'Examine'),
            ('unprocessed', 'Unprocessed'),
        ],
        default='unprocessed',
        help='Approved/Examine/Unprocessed - depending on whether employer was handled by case worker')

    application_ids = fields.One2many('casemanagement.application', 'employer_id', string='Applications')

    comment_ids = fields.One2many('casemanagement.comment', 'employer_id', string='Comments')

    # Has potentially many Approvals by Unions
    approval_ids = fields.One2many('casemanagement.approval', 'employer_id', string='Approvals')

    # Belongs to Campaign
    campaign_id = fields.Many2one('casemanagement.campaign', 'Campaign', readonly=True)

    @api.depends('approval_ids.status', 'approval_ids.valid_to_date')
    def _compute_union_approved(self):
        """
        Computes automatic union approval suggestion based on property values supplied in @api.depends decorator
        """
        today = datetime.now()
        for record in self:
            if record.approval_ids:
                # Approval from any Union makes Employer approved for 1 year
                if any([r.status == 'approved' and today < r.valid_to_date for r in record.approval_ids]):
                    record.union_approved = 'ok'
                elif any([[r.status == 'waiting' for r in record.approval_ids]]):
                    record.union_approved = 'waiting'
            else:
                record.union_approved = '--'

    @api.depends('approval_ids.status', 'approval_ids.valid_to_date')
    def _compute_union_approval_expiration_date(self):
        """
        Computes datetime when the latest Approval record was created
        """
        today = datetime.now()
        for record in self:
            if record.approval_ids:
                if any([r.status == 'approved' and today < r.valid_to_date for r in record.approval_ids]):
                    valid_to_dates = sorted([r.valid_to_date for r in record.approval_ids if r.valid_to_date])
                    record.union_approval_expiration_date = valid_to_dates[-1]
                else:
                    record.union_approval_expiration_date = None
            else:
                record.union_approval_expiration_date = None

    @api.depends('approval_ids.create_date')
    def _compute_union_approval_latest_date(self):
        """
        Computes date after which Union approval of Employer is no longer valid
        """
        for record in self:
            if record.approval_ids:
                create_dates = sorted([r.create_date for r in record.approval_ids])
                record.union_approval_latest_date = create_dates[-1]
            else:
                record.union_approval_latest_date = None

    @api.depends('name', 'org_number', 'email', 'no_payment_problem', 'no_tax_debt', 'no_bankruptcy', 'union_approved',
                 'bar_number')
    def _suggest_approval(self):
        """
        Computes automatic approval suggestion based on property values supplied in @api.depends decorator
        """
        for record in self:
            satisfied = all(
                [record.name, record.org_number, record.email, record.no_payment_problem, record.no_tax_debt,
                 record.no_bankruptcy, record.bar_number, record.union_approved == 'ok'])
            record.auto_suggestion_approval = 'approved' if satisfied else 'examine'

    @api.constrains('email')
    def _check_email(self):
        for record in self:
            if not valid_email(record.email):
                raise ValidationError("Field 'email' has to be a valid email address")

    @api.constrains('org_number')
    def _check_org_number(self):
        for record in self:
            if not valid_org_number(record.org_number):
                raise ValidationError("Field 'org_number' has to be a valid Swedish Organization Number")

    @api.model
    def employers_to_csv(self, employers):
        """
        Returns binary contents of CSV file with rows corresponding to Employer records
        :param employers: Employer records
        :type employers: list[Employer]
        :rtype: bytearray
        """
        rows = [[e.name, e.org_number, e.email] for e in employers]

        fp = io.BytesIO()
        writer = pycompat.csv_writer(fp, quoting=1)

        # Header - only these columns are present and allowed to be updated with CSV import
        writer.writerow(['name', 'org_number', 'email'])

        for data in rows:
            row = []
            for d in data:
                # Spreadsheet apps tend to detect formulas on leading =, + and -
                if isinstance(d, str) and d.startswith(('=', '-', '+')):
                    d = "'" + d

                row.append(pycompat.to_text(d))
            writer.writerow(row)

        return fp.getvalue()


class Application(models.Model, AttachmentHelper):
    _name = 'casemanagement.application'
    _description = 'Application for internship by Applicant connected to Employer'
    _order = "id desc"
    _translate = True

    name = fields.Char(help='Internship name')
    start_date = fields.Date(help='Start date')
    af_office = fields.Char(help='Public Employment Service local office')
    case_worker = fields.Char(help='Identifier of case worker handling this application')

    employer_id = fields.Many2one('casemanagement.employer', 'Employer')
    applicant_id = fields.Many2one('casemanagement.applicant', 'Applicant')

    applicant_registration_date = fields.Date(related='applicant_id.registration_date')
    applicant_residence_permit_type = fields.Selection(related='applicant_id.residence_permit_type')
    applicant_origin_country = fields.Selection(related='applicant_id.origin_country')

    auto_suggestion_approval = fields.Selection(
        compute='_suggest_approval',
        selection=[
            ('eligible', 'Eligible'),
            ('examine', 'Examine'),
        ],
        help='Eligible/Examine - automatically generated value based on Employer and Applicant property values',
        readonly=True
    )

    approval = fields.Selection(
        selection=[
            ('eligible', 'Eligible'),
            ('examine', 'Examine'),
            ('rejected', 'Rejected'),
            ('--', '--'),
        ],
        default='--',
        help='Eligible/Examine/-- - depending on whether Employer and Applicant were handled by case worker'
    )

    application_status = fields.Selection(
        selection=[
            ('waiting', 'Waiting'),
            ('in_progress', 'In Progress'),
            ('locked', 'Locked'),
            ('archived', 'Archived'),
        ],
        default='waiting',
        help='Waiting/In Progress/Locked/Archived - '
             'depending on whether case worker did any work on this application'
    )

    comment_ids = fields.One2many('casemanagement.comment', 'application_id', string='Comments')

    closed = fields.Boolean(
        compute='_compute_closed',
        default=False,
        help='Used to hide processed applications by default'
    )

    @api.depends('employer_id.approval', 'applicant_id.approval')
    def _suggest_approval(self):
        """
        Computes automatic approval suggestion based on property values supplied in @api.depends decorator
        """
        for record in self:
            # TODO: adjust & discuss logic
            # TODO: check that Applicant.country_id is outside EU
            if record.employer_id.approval == 'approved' and record.applicant_id.approval == 'eligible':
                record.auto_suggestion_approval = 'eligible'
            else:
                record.auto_suggestion_approval = 'examine'

    @api.depends('approval')
    def _compute_closed(self):
        """
        Computes value based on property values supplied in @api.depends decorator
        """
        for record in self:
            # TODO: adjust & discuss logic
            record.closed = record.approval in ['eligible', 'rejected']

    def mark_approved(self, *args):
        """
        Necessary logic for approving Application
        """
        records_to_update = self.get_records_to_update(*args)

        for record in records_to_update:
            # TODO: Save decision in AIS
            # TODO: Save to Diariet
            record.approval = 'eligible'

        return records_to_update

    def mark_rejected(self, *args):
        """
        Necessary logic for rejecting Application
        """
        records_to_update = self.get_records_to_update(*args)

        for record in records_to_update:
            # TODO: Save decision in AIS?
            # TODO: Save to Diariet?
            record.approval = 'rejected'

        return records_to_update


class Notification(models.Model):
    _name = 'casemanagement.notification'
    _description = 'Solid webhook notification update'
    _order = "id desc"
    _translate = True

    urn_uuid = fields.Char(
        help='ID property of JSON document',
        readonly=True)

    type = fields.Char(
        help='Type as string',
        readonly=True)

    object = fields.Char(
        help='Object as string',
        readonly=True)

    subscription_target = fields.Char(
        help='URI of object in Solid we get notified about',
        readonly=True)

    unsubscribe_endpoint = fields.Char(
        help='URL used to unsubscribe from further notifications',
        readonly=True)

    raw_json = fields.Char(
        help='JSON string payload received when notification from Solid arrived',
        readonly=True)

    published = fields.Datetime(
        help='Value of published property from Solid notification or current timestamp',
        readonly=True)

    @api.model_create_multi
    def create_from_notification_json(self, input_json):
        """
        Creates records from JSON received as Solid notification webhook
        :param input_json: Dict or list of dicts
        :type input_json: dict | list[dict]
        """
        if type(input_json) == dict:
            result = self.env['casemanagement.notification'].create(
                self._correct_notification_json(input_json)
            )
        elif type(input_json) == list:
            items = [self._correct_notification_json(i) for i in input_json]
            result = self.env['casemanagement.notification'].create(
                items
            )
        else:
            raise ValueError('Incorrect input_json type (expected dict|list): %s' % type(input_json))

        return result

    @staticmethod
    def _correct_notification_json(json_data):
        """
        Transforms notification received from Solid notification webhook into dict that can be sent to create()
        :param json_data: Dict to process
        :type json_data: dict
        """
        try:
            published_datetime_str = json_data.get('published')
            try:
                if published_datetime_str:
                    published_datetime = datetime.strptime(published_datetime_str, '%Y-%m-%dT%H:%M:%S.%fZ')
                else:
                    published_datetime = datetime.now()
            except Exception as e:
                _logger.error("Error '%s' when parsing value '%s' to datetime" % (e, published_datetime_str))
                published_datetime = datetime.now()

            return {
                'urn_uuid': json_data.get('id'),
                'type': json_data.get('type'),
                'object': json_data.get('object'),
                'subscription_target': json_data.get('object', dict()).get('id'),
                'unsubscribe_endpoint': json_data.get('unsubscribe_endpoint'),
                'raw_json': json.dumps(json_data),
                'published': published_datetime
            }
        except Exception as e:
            _logger.error(e)
            return {}


class WebhookSubscription(models.Model):
    _name = 'casemanagement.subscription'
    _description = 'Solid webhook subscription'
    _order = "id desc"
    _translate = True

    _sql_constraints = [
        ('digest_uniq', 'UNIQUE (digest)',
         'You can not have two Subscriptions with the same digest!')
    ]

    digest = fields.Char(
        help='Digest of JSON document creating subscription',
        readonly=True)

    type = fields.Char(
        help='Type as string',
        readonly=True)

    subscription_target = fields.Char(
        help='URI of object in Solid we get notified about',
        readonly=True)

    unsubscribe_endpoint = fields.Char(
        help='URL used to unsubscribe from further notifications',
        readonly=True)

    raw_json = fields.Char(
        help='JSON string payload received when subscription from Solid confirmed',
        readonly=True)

    def make_sure_subscribed(self):
        """
        Checks if a subscription to Solid Pod server inbox exists and creates it if not
        """
        subscription_target = f'{SOLID_SERVER_URL}/source/oak/inbox/'
        subscription = self.env['casemanagement.subscription'].search([
            ['subscription_target', 'like', subscription_target]
        ]).read()

        if subscription:
            _logger.info(f"Subscription to '{subscription_target}' exists")
        else:
            _logger.info(f"Subscription to '{subscription_target}' does not exist")
            # TODO: create subscription when client credentials authentication flow is possible
            session = dict()
            from ..util.solid import subscribe_resource
            subscribe_resource(subscription_target, session)


class Comment(models.Model):
    _name = 'casemanagement.comment'
    _description = 'Comment attached to record'
    _order = "id desc"
    _translate = True

    comment_text = fields.Text(help='Comment text')

    attachment_ids = fields.One2many('casemanagement.attachment', 'comment_id', string='Attachments')

    # Can belong to any of the below
    applicant_id = fields.Many2one('casemanagement.applicant', 'Applicant', readonly=True)
    employer_id = fields.Many2one('casemanagement.employer', 'Employer', readonly=True)
    application_id = fields.Many2one('casemanagement.application', 'Application', readonly=True)


class Attachment(models.Model):
    _name = 'casemanagement.attachment'
    _description = 'File attached to record'
    _order = 'id desc'
    _translate = True

    name = fields.Char(help='File name including extension')
    base64_data = fields.Binary(help='File binary contents')
    mimetype = fields.Char(help='File mimetype')
    download_url = fields.Char(help='Download URL', compute="_compute_download_url")

    # Belongs to Comment
    comment_id = fields.Many2one('casemanagement.comment', 'Comment', readonly=True)

    @api.depends('name')
    def _compute_download_url(self):
        for record in self:
            record.download_url = f'/casemanagement/attachment/download/{record.id}'

    @api.model_create_multi
    def create(self, vals_list):
        for val in vals_list:
            if not val.get('mimetype'):
                val['mimetype'] = self.guess_mimetype(val)

        res_ids = super(Attachment, self).create(vals_list)

        return res_ids

    @staticmethod
    def guess_mimetype(record):
        """
        Guess mimetype based on file name first and on binary data second, if mimetype value not defined
        :param record: Dict with values for Attachment record
        :rtype: str
        """
        mimetype = record.get('mimetype')
        file_name = record.get('name')
        data = record.get('base64_data')

        if not mimetype and file_name and '.' in file_name:
            mimetype = mimetypes.guess_type(file_name)[0]

        if not mimetype and data:
            mimetype = guess_mimetype(base64.b64decode(data))

        return mimetype or 'application/octet-stream'


class Import(models.Model):
    _name = 'casemanagement.import'
    _description = 'CSV import of Employers'
    _order = 'id desc'
    _translate = True

    file_name = fields.Char(help='File name including extension')
    csv_file = fields.Binary(help='CSV import file binary contents (base64-encoded string)')
    import_result = fields.Text(help='Result of import as text')

    # Belongs to Campaign
    campaign_id = fields.Many2one('casemanagement.campaign', 'Campaign', readonly=True)


class Campaign(models.Model):
    _name = 'casemanagement.campaign'
    _description = 'Campaign holding Import and valid until date_end'
    _order = 'id desc'
    _translate = True
    _sql_constraints = [
        ('date_end_uniq', 'UNIQUE (date_end)', 'You can not have two Campaigns with the same date_end!'),
    ]

    name = fields.Char(help='Campaign name')
    date_end = fields.Date(help='Campaign valid until')

    import_ids = fields.One2many('casemanagement.import', 'campaign_id', string='Imports')
    employer_ids = fields.One2many('casemanagement.employer', 'campaign_id', string='Employers')

    @api.model
    def create_union_approvals(self, today=None):
        """
        Create UnionApproval records for every employer_ids record in this Campaign
        :param today: Datetime to use as create_date, used in testing
        :type today: datetime
        """
        if len(self):
            # Create UUID per Union record
            trade_unions = self.env['casemanagement.union'].search([])
            guid_mapping = [{'guid': str(uuid.uuid4().hex),
                             'id': u.id,
                             'union': u} for u in trade_unions]
            _logger.info(guid_mapping)
        else:
            guid_mapping = []

        for record in self:
            # Connected Employer records
            for employer in record.employer_ids:
                for u in guid_mapping:
                    created_approval = self.env['casemanagement.approval'].create({
                        'guid': u.get('guid'),
                        'union_id': u.get('id'),
                        'employer_id': employer.id
                    })
                    if today:
                        created_approval.write({'create_date': today})

        return guid_mapping

    @api.constrains('date_end')
    def _check_date_end(self):
        """
        Date end should not be in the past
        :return:
        """
        for record in self:
            if record.date_end < fields.Date.today():
                raise ValidationError("Field 'date_end' cannot be a date in the past")

    @api.model
    def remove_old_data(self, today=None, domain=None):
        """
        Remove old Applicant and Application records after 2 months (62 days) have passes since latest Campaign ended
        :param today: Date object to use as "today" for testing that correct records are deleted
        :type today: datetime | date
        :param domain: Additional filter used on records to be deleted
        :type domain: list
        :rtype: tuple(int, int)
        """
        # Campaign is used to get the timespan indicating what Applicant and Application records will be deleted
        today = today or fields.Date.today()
        two_months_back = today - timedelta(days=62)
        expired_campaign = self.env[self._name].search([['date_end', '<', two_months_back]])
        removed_applicants = 0
        removed_applications = 0

        for campaign in expired_campaign:
            if domain:
                combined_domain = [domain,
                                   ['create_date', '<', campaign.date_end]]
            else:
                combined_domain = [['create_date', '<', campaign.date_end]]

            # Remove Applicants
            old_applicants = self.env['casemanagement.applicant'].search(combined_domain)
            if old_applicants:
                _logger.info(f'Removing {len(old_applicants)} Applicant records created before {campaign.date_end}')
                old_applicants.unlink()
                removed_applicants += len(old_applicants)

            # Remove Applications created before campaign date_end
            old_applications = self.env['casemanagement.application'].search(combined_domain)
            if old_applications:
                _logger.info(f'Removing {len(old_applications)} Application records created before {campaign.date_end}')
                old_applications.unlink()
                removed_applications += len(old_applications)

        if not expired_campaign:
            _logger.info(f'No Campaign with date_end older than "{two_months_back}" was found')

        return removed_applicants, removed_applications

    @api.model
    def update_approvals_for_soon_invalid_employers(self, today=None, expire_before_date=None, ignore_recent=False):
        """
        Creates new UnionApproval records for Employers whose valid status expires sooner than 30 days
        :param today: Date object to use as "today" for testing that correct records are selected
        :type today: datetime
        :param expire_before_date: Datetime object to use to find Employer records whose validity expires before
        :type expire_before_date: datetime
        :param ignore_recent: If True, ignores recently create UnionApproval records and creates new ones
        :type ignore_recent: bool
        :returns: Number of unique emails sent to Union representatives
        """
        # TODO: discuss logic and hard-coded 30 days
        today = today or fields.Datetime.now()
        one_month_forward = expire_before_date or today + timedelta(days=30)
        num_emails = 0

        # Select active Campaign(s)
        campaigns = self.env[self._name].search([['date_end', '>=', today]])

        # For every Campaign, check if validity expires in less than 30 days
        for c in campaigns:
            # For all Employer records in Campaign, create new UnionApproval records,
            # if at least one Employer validity expires within 30 days
            if any([e.union_approval_expiration_date and e.union_approval_expiration_date < one_month_forward
                    for e in c.employer_ids]):
                today_back_days = today - timedelta(days=30)
                # Avoid creating UnionApproval records and sending emails more often than once per 30 days
                approvals_old = [e.union_approval_latest_date and e.union_approval_latest_date < today_back_days
                                 for e in c.employer_ids]
                if ignore_recent or all(approvals_old):
                    guid_mapping = c.create_union_approvals(today=today)
                    # Send emails to all Unions
                    send_emails_to_unions(guid_mapping)
                    num_emails += len(guid_mapping)

        if not num_emails:
            _logger.info('No UnionApproval records with validity expiring soon found')

        return num_emails


class UnionApproval(models.Model):
    _name = 'casemanagement.approval'
    _description = 'Trade union approval valid for some time issued for a specific Employer'
    _order = 'id desc'
    _translate = True

    signed_by = fields.Char(help='Personal number of a person signing/approving Employer', readonly=True)
    signed_date = fields.Datetime(help='Signed/approved by Union representative on', readonly=True)
    valid_to_date = fields.Datetime(help='Approval valid to this date', readonly=True)
    guid = fields.Char(help='Unique identifier used in URL for Unions', readonly=True)
    status = fields.Selection(
        selection=[
            ('waiting', 'Waiting'),
            ('approved', 'Approved'),
        ],
        default='waiting',
        help='Waiting/Approved - waiting for Union to approve, approved by Union representative'
    )

    union_id = fields.Many2one('casemanagement.union', 'Union', readonly=True)
    employer_id = fields.Many2one('casemanagement.employer', 'Employer', readonly=True)

    @api.constrains('signed_by')
    def _check_signed_by(self):
        for record in self:
            if not valid_org_number(record.signed_by):
                raise ValidationError("Field 'signed_by' has to be a valid Swedish personal number")

    def sign_approval(self, personal_number):
        signed_date = fields.Datetime.now()
        valid_to_date = signed_date.replace(year=signed_date.year+1)
        for record in self:
            record.signed_by = personal_number
            record.signed_date = signed_date
            record.valid_to_date = valid_to_date
            record.status = 'approved'


class TradeUnion(models.Model):
    _name = 'casemanagement.union'
    _description = 'Trade union'
    _order = 'id desc'
    _translate = True

    name = fields.Char(help='Trade union name', required=True)
    email = fields.Char(help='Email address of a representative', required=True)

    approval_ids = fields.One2many('casemanagement.approval', 'union_id', string='Approvals')

    @api.constrains('email')
    def _check_email(self):
        for record in self:
            if not valid_email(record.email):
                raise ValidationError("Field 'email' has to be a valid email address")

    @api.model
    def update_email(self, **post):
        """
        Updates email addresses for Union records
        :param post: Data to update email addresses from
        :rtype: list[TradeUnion]
        """
        updated_unions = list()
        for k, v in post.items():
            union_id = int(k.replace('email_', ''))
            union = self.env[self._name].browse([union_id])
            if valid_email(v) and union.email != v:
                union.update({'email': v})
                updated_unions.append(union)

        return updated_unions


class ClassificationCode(models.Model):
    _name = 'casemanagement.code'
    _description = 'Classification code from Swedish Migration Board'
    _order = 'id desc'
    _translate = True
    _sql_constraints = [
        ('name_uniq', 'UNIQUE (name)',
         'You can not have two codes with the same name!'),
    ]

    name = fields.Char(help='Classification code', required=True)
    type = fields.Selection(
        selection=[
            ('active', 'Active'),
            ('passive', 'Passive'),
        ],
        default='passive',
        help='Passive - not issued any longer, Active - can still be issued'
    )
    description = fields.Char(help='Description')
    note = fields.Char(help='Note')
    legal_text = fields.Char(help='Legal text')
    eligible = fields.Selection(
        selection=[
            ('true', 'True'),
            ('false', 'False'),
            ('undecided', 'Undecided'),
        ],
        default='undecided',
        help='Eligible or not for JobbSprånget case'
    )

    applicant_ids = fields.One2many('casemanagement.applicant', 'classification_code_id', string='Applicants')


def valid_org_number(org_number):
    """
    Checks if org_number does not contain invalid characters, only digits and "-" allowed
    :param org_number: Email address string
    :type org_number: str
    :rtype: bool
    """
    return re.fullmatch(r'[0-9-]+', str(org_number))
