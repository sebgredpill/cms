# -*- coding: utf-8 -*-
{
    'name': "case_management",
    'summary': "Case management system for Arbetsförmedlingen",
    'description': "Help case workers dealing with applications for internships",
    'author': "Jobtechdev",
    'website': "https://jobtechdev.se/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '15.0.1.0.0',
    'license': 'AGPL-3',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # external dependencies, installation terminates if not satisfied
    'external_dependencies': {
       'python': ['jwcrypto', 'rdflib', 'solidclient'],
    },

    # outputs installed packages, used for debugging only
    'pre_init_hook': 'check_dependencies',

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/templates.xml',
        'data/views.xml',
        'data/casemanagement.employer.csv',
        'data/casemanagement.applicant.csv',
        'data/casemanagement.application.csv',
        'data/casemanagement.union.csv',
        'data/casemanagement.code.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
