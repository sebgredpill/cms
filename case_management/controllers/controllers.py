# -*- coding: utf-8 -*-
import base64
import datetime
import functools
import logging
import os

import requests
import werkzeug
from dateutil import tz
from werkzeug import Response

from odoo.tools import date_utils
from ..models.models import valid_org_number
from ..util.api import migration_details_by_seeker_id

from odoo import http
from odoo.http import request, content_disposition
import math
import json
import jwcrypto.jwk
import jwcrypto
import jwcrypto.jwt
from solidclient.solid_client import SolidSession, SolidAPIWrapper
from ..util.email import send_email, send_email_with_attachment, extract_attachment_data, JS_EMAIL, \
    send_emails_to_unions
from ..util.rdf import collect_rdf_graph_data
from ..util.solid import get_digest, put_response_to_solid, get_key_and_access_token_from_file, \
    write_key_and_access_token_to_file, solid_client_credentials_login, get_authenticated_solid_session
from ..util.vc import NodeVC

_logger = logging.getLogger(__name__)

# Constants
SOLID_SERVER_URL = os.environ.get("SOLID_SERVER_URL", "http://localhost:3000")
SECRET_SOLID_SERVER_EMAIL = os.environ.get("SECRET_SOLID_SERVER_EMAIL", "source@example.com")
SECRET_SOLID_SERVER_PW = os.environ.get("SECRET_SOLID_SERVER_PW", "source")
APP_SERVER_URL = os.environ.get("APP_SERVER_URL", "http://localhost:8069/")
DATA_FILES = ["bolagsverket.ttl", "migrationsverket.ttl", "skatteverket.ttl"]
NODE_VC_SCRIPT_PATH = os.environ.get("NODE_VC_SCRIPT_PATH", '/var/oak/solid-client-node-experimentation/issue-vc.js')


def serialize_exception(f):
    @functools.wraps(f)
    def wrap(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            _logger.exception("An exception occurred during an HTTP request")
            se = http.serialize_exception(e)
            error = {
                'code': 200,
                'message': "Odoo Server Error",
                'data': se
            }
            return werkzeug.exceptions.InternalServerError(json.dumps(error, indent=4))

    return wrap


class CaseManagement(http.Controller):

    @staticmethod
    def pop_from_session(session_key):
        """
        Helper for retrieving a value from session
        :param session_key: Session key to retrieve value for
        """
        return request.session.pop(session_key, None)

    @staticmethod
    def pop_flash_from_session(session_key="cm_flash"):
        """
        Helper for retrieving flash message from session
        :param session_key: Session key to retrieve value for
        """
        return CaseManagement.pop_from_session(session_key)

    @staticmethod
    def set_flash_in_session(message, session_key="cm_flash"):
        """
        Helper for setting flash message in session
        :param message: Message to save in session
        :param session_key: Session key to use to store message at
        """
        request.session[session_key] = message

    @http.route('/casemanagement', auth='user', website=True)
    def index(self, page=1, applications_per_page=25, **kw):
        # TODO: modal_message passed via session
        current_page = page  # Grab from query param

        applications_per_page_int = int(applications_per_page)

        db_offset = (int(page) - 1) * int(applications_per_page)

        applications = http.request.env["casemanagement.application"] \
            .search([['approval', 'not in', ['approved', 'rejected']]],
                    limit=applications_per_page_int,
                    offset=db_offset)

        total_applications = http.request.env["casemanagement.application"] \
            .search([['approval', 'not in', ['approved', 'rejected']]], count=True)

        pages = math.ceil(total_applications / applications_per_page_int)

        return http.request.render("case_management.casemanagementgrid", {
            "root": "/casemanagement",
            "applications": applications,
            "tot_pages": pages,
            "current_page": int(current_page),
            "modal_message": self.pop_flash_from_session(),
        })

    @http.route("/casemanagement/fetch-applicant-data", auth="user", website=True)
    def fetch_applicant_data(self, username, **post):
        _logger.info(f"Username '{username}' fetching data")

        # What do we do here?
        # 1. Load private key and access token from file for the sink user
        # 2. Try and grab data from [
        # SOLID_SERVER_URL/{username}/share/migrationsverket.ttl
        # SOLID_SERVER_URL/{username}/share/bolagsverket.ttl
        # SOLID_SERVER_URL/{username}/share/skatteverket.ttl
        # ]
        # and put it into our db

        solid_session = get_authenticated_solid_session(request.session)

        collected_data = {}

        for file in DATA_FILES:
            url = f"{SOLID_SERVER_URL}/{username}/share/{file}"
            resp = solid_session.get(url)
            _logger.info("url '{}' status code: {}".format(url, resp.status_code))

            if resp.status_code == 200:
                # rdf_graph = resp.get_graph()
                _logger.info("OLD response:")
                _logger.info(resp.raw_text)

                collect_rdf_graph_data(collected_data, resp.raw_text)
            elif resp.status_code == 401:
                solid_client_credentials_login(request.session, overwrite_existing_token=True)
                if 'solid_access_token_response' in request.session:
                    return self.fetch_applicant_data(username, **post)
            elif resp.status_code == 404:
                _logger.info(f"Resource '{url}' not found (404)")

            if 'solid_access_token_response' in request.session:
                new_solid_session = SolidAPIWrapper(client_id=request.session.get('solid_client_id'),
                                                    client_secret=request.session.get('solid_client_secret'),
                                                    access_token=request.session.get('solid_access_token_response'),
                                                    keypair=request.session.get('solid_key'),
                                                    logger=_logger)
                new_resp = new_solid_session.get(url)
                _logger.info("NEW response:")
                _logger.info(new_resp.text)

        _logger.info(f"collected_data {collected_data}")

        modal_message = "No personal_number or name in collected_data"
        # Create an applicant in the db based on the variables.
        # In order to create an applicant we need at least a personal number and a name
        if "personal_number" in collected_data and "first_name" in collected_data and "last_name" in collected_data:
            try:
                # TODO: prepare data before sending to Applicant
                collected_data.pop("employer_good")
                collected_data.pop("employer")
                collected_data.pop("residence_permit_type")

                # TODO: Normalize personal number? Is it always in the same format?
                applicant = http.request.env["casemanagement.applicant"].search(
                    [['personal_number', '=', collected_data.get('personal_number')]]
                )
                if applicant:
                    # Update existing applicant
                    applicant[0].update(collected_data)
                    _logger.info(f"Updated applicant with data {json.dumps(collected_data, indent=4)}")
                    modal_message = "Updated Applicant '{} {}' ({})".format(collected_data["first_name"],
                                                                            collected_data["last_name"],
                                                                            collected_data["personal_number"])
                else:
                    # Create new applicant
                    http.request.env["casemanagement.applicant"].create(collected_data)
                    _logger.info(f"Created applicant with data {json.dumps(collected_data, indent=4)}")
                    modal_message = "Created Applicant '{} {}' ({})".format(collected_data["first_name"],
                                                                            collected_data["last_name"],
                                                                            collected_data["personal_number"])
            except Exception as e:
                _logger.error(e)
                modal_message = str(e)

        self.set_flash_in_session(modal_message)

        return self.index()

    @http.route("/casemanagement/oauth/callback",
                methods=["GET"],
                auth="user",
                website=True)
    def oauth_callback(self, **kwargs):
        """
        This was used before client credentials authentication flow was available in Solid IdP
        """
        auth_code = kwargs.get('code')
        state = kwargs.get('state')
        assert state in request.session, f"state {state} not in request.session?"

        # Generate a key-pair.
        keypair = jwcrypto.jwk.JWK.generate(kty='EC', crv='P-256')
        solid_session = SolidSession(keypair, state_storage=request.session)

        code_verifier = request.session[state].pop('solid_code_verifier')

        basic_secret = base64.b64encode(
            (request.session['solid_client_id'] + ':' + request.session['solid_client_secret']).encode('utf-8'))

        # Exchange auth code for access token
        resp = requests.post(url=solid_session.provider_info['token_endpoint'],
                             data={
                                 "grant_type": "authorization_code",
                                 "client_id": request.session['solid_client_id'],
                                 "redirect_uri": solid_session.get_callback_url(),
                                 "code": auth_code,
                                 "code_verifier": code_verifier,
                             },
                             headers={
                                 'Authorization': 'Basic ' + basic_secret.decode('utf-8'),
                                 'DPoP': solid_session.make_token(keypair,
                                                                  solid_session.provider_info['token_endpoint'],
                                                                  'POST')
                             },
                             allow_redirects=True)
        result = resp.json()

        _logger.info('++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        _logger.info('Access token response:')
        _logger.info(json.dumps(result, indent=4))

        request.session['solid_key'] = keypair.export()
        request.session['solid_access_token'] = result['access_token']
        request.session['solid_access_token_response'] = result

        write_key_and_access_token_to_file(keypair.export(), result["access_token"], result,
                                           request.session['solid_client_id'],
                                           request.session['solid_client_secret'])

        self.set_flash_in_session("Authentication with OIDC successful! You can now access Solid resources.")

        state = request.session.pop(state)

        return request.redirect(state.pop('solid_redirect_url'))

    @http.route("/casemanagement/upload-employers",
                methods=["GET"],
                auth="user",
                website=True)
    def upload_csv_file_get(self, modal_message=None):
        existing_campaigns = http.request.env['casemanagement.campaign'].search([])
        latest_campaign = None
        default_date = None
        latest_import = None
        latest_import_date = None
        if existing_campaigns:
            latest_campaign = existing_campaigns[0]
            default_date = latest_campaign.date_end
            latest_import = latest_campaign.import_ids[0] if latest_campaign.import_ids else None
            if latest_import:
                import_date = latest_import.create_date
                latest_import_date = get_local_datetime(import_date).strftime('%Y-%m-%d %H:%M:%S')

        return http.request.render('case_management.upload-employer-csv',
                                   {'modal_message': modal_message,
                                    'latest_campaign': latest_campaign,
                                    'latest_import': latest_import,
                                    'latest_import_date': latest_import_date,
                                    'default_date': default_date})

    @http.route("/casemanagement/upload-employer-csv",
                methods=["POST"],
                auth="user",
                website=True)
    @serialize_exception
    def upload_csv_file_post(self, **post):
        """
        Process employer records found in CSV file, redirects to Applications list
        """
        campaign_end = post.get('campaign_end')
        if not campaign_end:
            return self.upload_csv_file_get('Please fill in campaign end date')

        parsed_campaign_end = validate_campaign_date_end(campaign_end)
        if not parsed_campaign_end:
            return self.upload_csv_file_get('Campaign end date is invalid')

        # Extract file object
        attachment_data = extract_attachment_data(post, 'csv_file')

        file_name = attachment_data.get('name', 'no_name.csv')
        csv_file_binary = attachment_data.get('binary', b'')

        try:
            # Create an import object to work with
            import_wizard = http.request.env['base_import.import'].create({
                'res_model': 'casemanagement.employer',
                'file': csv_file_binary,
                'file_type': 'text/csv',
                'file_name': file_name,
            })
            fields = ["name", "org_number", "email"]
            options = {'quoting': '"', 'separator': ',', 'has_headers': True}

            # Actual import is done here
            result = import_wizard.execute_import(
                fields,
                [],
                options,
                dryrun=False
            )
        except Exception as e:
            _logger.error(f'Error in CSV import of Employers (problem with CSV?): "{e}"')
            return self.upload_csv_file_get('File is not valid, please check it and try again.')

        import_message = 'CSV file processed! '
        created_employer_ids = result['ids']
        if created_employer_ids:
            import_message += f"{len(created_employer_ids)} Employers created. " \
                              f"Result:\n\n{json.dumps(result, indent=4)}"

        # Handle potential updates to existing Employers
        # If any of the records could not be imported, the whole transaction is rolled back, so we
        # update/import one by one in update_existing_employers()
        errors = result['messages']
        if errors:
            result_preview = import_wizard.parse_preview({
                'quoting': '"',
                'separator': ',',
                'has_headers': True,
            })

            if 'error' in result_preview:
                error = result_preview['error']
                import_message = f'Problem with file: "{error}". Please check it and try again.'
                _logger.error(import_message)
            else:
                updated_employers = process_employers(import_wizard, fields, options)
                # Both updated and created are in the list below
                created_employer_ids = [e.id for e in updated_employers]
                import_message += f"{len(updated_employers)} Employers updated/created: \n" \
                                  f"{json.dumps([f'{e.name} ({e.org_number})' for e in updated_employers], indent=4)}"
        else:
            result_preview = dict()

        _logger.info(import_message)

        # Save Import and Campaign
        existing_campaign = http.request.env['casemanagement.campaign'].search([
            ['date_end', '=', parsed_campaign_end.date()]
        ])
        # TODO: discuss update/delete/create logic for existing Campaign
        if existing_campaign:
            created_campaign = existing_campaign[0]
        else:
            created_campaign = http.request.env['casemanagement.campaign'].create({
                'date_end': parsed_campaign_end.date()})

        # Update Employers that were created to belong to this Campaign
        if created_employer_ids:
            created_employers = http.request.env['casemanagement.employer'].search([['id', 'in', created_employer_ids]])
            created_employers.update({'campaign_id': created_campaign.id})

        # Save Import for reference/history/debugging
        http.request.env['casemanagement.import'].create({
            'file_name': file_name,
            'csv_file': base64.b64encode(csv_file_binary),
            'import_result': json.dumps(dict(result=result, preview=result_preview), indent=4),
            'campaign_id': created_campaign.id
        })

        # self.set_flash_in_session(import_message)

        # Possible to redirect to existing Actions passing action ID as action parameter
        # return request.redirect("/web#action=case_management.show_employers")
        return self.upload_csv_file_get(import_message)

    @http.route("/casemanagement/send-campaign-to-unions",
                methods=["POST"],
                auth="user",
                website=True)
    @serialize_exception
    def send_campaign_to_unions(self, **post):
        """
        Create Approval records, send emails with unique links for every Union
        """
        campaign_id = post.get('campaign_id')
        if not campaign_id:
            return self.upload_csv_file_get('No Campaign found to send to Unions!')

        campaign = http.request.env['casemanagement.campaign'].search([['id', 'in', [campaign_id]]])
        if not campaign:
            return self.upload_csv_file_get('No Campaign found to send to Unions!')

        # Create Approvals
        guid_mapping = campaign.create_union_approvals()
        if not guid_mapping or not campaign.employer_ids:
            return self.upload_csv_file_get('No Unions or Employers found!')

        send_emails_to_unions(guid_mapping)

        return self.upload_csv_file_get(f'Emails sent to {len(guid_mapping)} Unions')

    @http.route("/casemanagement/migration/<int:job_seeker_id>",
                methods=["GET"],
                auth="user",
                website=True)
    def get_migration_details(self, job_seeker_id):
        """
        Get information from API endpoint
        :param job_seeker_id: AFs internal job seeker ID
        :type job_seeker_id: int
        """
        result = json.dumps(migration_details_by_seeker_id(job_seeker_id), indent=4)
        _logger.info("Migration Details:")
        _logger.info(result)

        return http.request.render('case_management.get-migration-details', {"result": result})

    @http.route("/casemanagement/<model_name>/<int:record_id>",
                methods=["GET"],
                auth="user")
    @serialize_exception
    def get_record_by_id(self, model_name, record_id):
        """
        Get Employer by ID
        :param record_id: Employer ID
        :type record_id: int
        :param model_name: Model name in casemanagement module (employer/applicant/application)
        :type model_name: str
        """
        assert model_name in ['applicant', 'application', 'employer']
        try:
            fetched_employer = http.request.env[f'casemanagement.{model_name}'].browse(
                [record_id]
            ).read()

            # Datetime values can not be serialized out of the box, so odoo provides a serializer
            json_result = json.dumps(fetched_employer, indent=4, default=date_utils.json_default)
            response = Response(json_result, content_type="application/json")
        except Exception as e:
            _logger.error(f"Error '{e}' when fetching record '{model_name}' by ID '{record_id}'")
            response = Response("[]", content_type="application/json")

        return response

    @http.route("/casemanagement/<model_name>/",
                methods=["GET"],
                auth="user")
    @serialize_exception
    def get_all_records(self, model_name):
        """
        Get all records of a given model
        :param model_name: Model name in casemanagement module (employer/applicant/application)
        :type model_name: str
        """
        assert model_name in ['applicant', 'application', 'employer']
        try:
            domain = []
            if model_name == 'application':
                # filter out processed Application records
                domain.append(['approval', 'not in', ['approved', 'rejected']])

            fetched_records = http.request.env[f'casemanagement.{model_name}'].search(
                domain
            ).read()

            # Datetime values can not be serialized out of the box, so odoo provides a serializer
            json_result = json.dumps(fetched_records, indent=4, default=date_utils.json_default)
            response = Response(json_result, content_type="application/json")
        except Exception as e:
            _logger.error(f"Error '{e}' when fetching all records from model '{model_name}'")
            response = Response("[]", content_type="application/json")

        return response

    @http.route('/casemanagement/solid-notification-webhook',
                methods=['POST'],
                auth='public',
                type='json')
    @serialize_exception
    def solid_notification_webhook(self):
        response_json = dict(status="OK")
        try:
            # TODO: logic for unsubscribing
            # TODO: trigger update of Applicant and Application update
            # TODO: update Subscription model
            webhook_json = http.request.jsonrequest
            if webhook_json and 'object' in webhook_json and 'unsubscribe_endpoint' in webhook_json:
                _logger.info("=================================================================================")
                _logger.info(webhook_json)
                _logger.info("=================================================================================")
                _logger.info("Object: '{}', unsubscribe_endpoint: '{}'".format(webhook_json['object'],
                                                                               webhook_json['unsubscribe_endpoint']))

                # Verify signature from Pod server
                headers = http.request.httprequest.headers
                signed_jwt = headers.get('Authorization')
                if not _validate_notification_signature(signed_jwt):
                    raise ValueError('Solid Pod Server notification signature validation failed')

                # Save Notification
                http.request.env['casemanagement.notification'].create_from_notification_json(
                    webhook_json
                )

                # TODO: do something with notification object
                key, access_token, raw_token, client_id, client_secret, valid_until = get_key_and_access_token_from_file()
                url = webhook_json['object']['id']

                solid_session = SolidAPIWrapper(client_id=client_id,
                                                client_secret=client_secret,
                                                access_token=raw_token,
                                                keypair=key,
                                                logger=_logger)
                resp = solid_session.get(url)
                _logger.info("=================================================================================")
                _logger.info(resp.text)
                _logger.info("=================================================================================")
                _logger.info("Fetched resource URL '{}', status code: {}".format(url, resp.status_code))
                _logger.info("=================================================================================")

                # Demo example
                if '/oak/inbox' in url and webhook_json['type'][0] == 'Create':
                    _logger.info("============================ DEMO 1 ============================")
                    _logger.info(resp.text)

                    collected_data = dict()
                    collect_rdf_graph_data(collected_data, resp.text)
                    if 'data_request' in collected_data:
                        # Fetch data request, get subject personal number
                        data_request_resp = solid_session.get(collected_data.get('data_request'))
                        collect_rdf_graph_data(collected_data, data_request_resp.text)
                        if 'first_name' not in collected_data:
                            collected_data['first_name'] = 'DEMO'
                            collected_data['last_name'] = collected_data.get('identifier')

                    _logger.info("============================ DEMO 2 ============================")
                    _logger.info(collected_data)

                    # Extract data used when creating Applicant
                    personal_number = collected_data.get('personal_number')
                    solid_guid = collected_data.get('identifier')

                    # Create Applicant (if not exists) based on personal number in Notification, save solid_guid
                    applicant = http.request.env["casemanagement.applicant"].search(
                        [['personal_number', '=', personal_number]]
                    )
                    if applicant:
                        # TODO: Update existing applicant?
                        # applicant[0].update(collected_data)
                        _logger.info(f"Updated applicant with data '{collected_data}'")
                    else:
                        # Create new applicant
                        created_applicant = http.request.env["casemanagement.applicant"].create({
                            'personal_number': personal_number,
                            'first_name': collected_data.get('first_name'),
                            'last_name': collected_data.get('last_name'),
                            'solid_guid': solid_guid
                        })
                        # Demo workflow
                        if collected_data.get('first_name') == 'DEMO':
                            # Update demo applicant with good values
                            created_applicant.update({
                                'phone': '0701234567',
                                'email': 'm@gmail.com',
                                'residence_permit_type': 'put',
                                'residence_permit_type_good': True,
                                'origin_country': 'Ukraine',
                            })
                            # Create Application with existing Employer and created applicant above
                            http.request.env["casemanagement.application"].create({
                                'name': 'DEMO Application',
                                'start_date': '2022-06-01',
                                'employer_id': 1,
                                'applicant_id': created_applicant.id
                            })

                    # put_response_to_solid(personal_number, solid_guid)

            else:
                error = "'object' or 'unsubscribe_endpoint' not found in request JSON"
                _logger.error(f"Error processing notification webhook: {error}")
                return Response(status=400)
        except Exception as e:
            _logger.error(f"Error processing notification webhook: '{type(e)}' - '{e}'")
            return Response(status=400)

        # Solid server is not smart enough at the moment, but return status 200 OK if went well and 400 if error
        return response_json

    @http.route("/casemanagement/subscribe-to-solid-resource/",
                auth="user",
                website=True)
    def subscribe_to_solid_resource(self, solid_resource_uri=None, **post):
        """
        Subscribe to a Solid resource for WebHookSubscription2021 notifications
        :param solid_resource_uri: Solid resource URI to receive notifications for
        :type solid_resource_uri: str
        """

        solid_resource_uri = post.get('solid_resource_uri') or solid_resource_uri
        subscription_resource = solid_resource_uri

        try:
            solid_session = get_authenticated_solid_session(request.session)

            # Subscribe to a resource on Solid server
            if request.session.get('solid_subscription_resource_uri'):
                subscription_resource = request.session.pop('solid_subscription_resource_uri')

            if subscription_resource:
                _logger.info("==============================================")
                _logger.info(f"Subscribing to notifications on resource '{subscription_resource}'")

                # Negotiate for WebHook protocol
                gateway_data = {
                    "@context": ["https://www.w3.org/ns/solid/notification/v1"],
                    "type": ["WebHookSubscription2021"],
                    "features": ["state", "webhook-auth"],
                }
                # Usually /gateway
                notification_endpoint = solid_session.solid_metadata.get('notification_endpoint')
                gateway_response = solid_session.post(notification_endpoint,
                                                      json.dumps(gateway_data)).json()
                _logger.info("====================== Gateway response ========================")
                _logger.info(gateway_response)

                # Usually /subscription
                subscription_endpoint = gateway_response.get('endpoint')

                # Request new WebHook subscription
                subscription_data = {
                    "@context": ["https://www.w3.org/ns/solid/notification/v1"],
                    "type": "WebHookSubscription2021",
                    "topic": subscription_resource,
                    "target": f"{APP_SERVER_URL}casemanagement/solid-notification-webhook",
                }
                # TODO: Check if not subscribed yet
                # Prepare data for Subscription object
                sub_data = dict(digest=get_digest(subscription_data))
                sub_data.update(subscription_data)
                found_subscription = http.request.env['casemanagement.subscription'] \
                    .search([['digest', '=', sub_data.get('digest')]], count=True)
                if not found_subscription:
                    subscription_response = solid_session.post(subscription_endpoint,
                                                               json.dumps(subscription_data))
                    _logger.info("====================== Subscription response ========================")
                    _logger.info(subscription_response.raw_text)

                    if subscription_response.status_code != 200 and "No WebId present" in subscription_response.raw_text:
                        # Key and access_token most likely expired, start auth and redirect back to this endpoint
                        request.session['solid_subscription_resource_uri'] = subscription_resource
                        # Forget expired key and access_token
                        request.session.pop('solid_key')
                        request.session.pop('solid_access_token')

                        # Perform login to Solid Pod server, extract and save key and access token
                        solid_client_credentials_login(request.session)
                        return self.subscribe_to_solid_resource(subscription_resource, **post)

                    data = {
                        'digest': sub_data.get('digest'),
                        'type': sub_data.get('type'),
                        'subscription_target': sub_data.get('topic'),
                        'unsubscribe_endpoint': subscription_response.json().get('unsubscribe_endpoint'),
                        'raw_json': json.dumps(subscription_response.json()),
                    }
                    http.request.env['casemanagement.subscription'].create(data)

                    modal_message = "Subscribed to resource '{}'; response: '{}'".format(subscription_resource,
                                                                                         subscription_response.raw_text)
                else:
                    modal_message = "Tried to subscribe to resource '{}', but Subscription " \
                                    "with identical digest already exists".format(subscription_resource)
                    _logger.info("==============================================")
                    _logger.info(modal_message)
            else:
                modal_message = "No notification subscription resource found"
                _logger.info(modal_message)
        except Exception as e:
            modal_message = f"Error when subscribing to Solid resource '{subscription_resource}': '{e}'"
            _logger.error(modal_message)

        self.set_flash_in_session(modal_message)

        return self.index()

    @http.route('/casemanagement/application/approve/<int:application_id>',
                methods=['POST'],
                auth='user',
                type='json')
    @serialize_exception
    def application_approve(self, application_id):
        """
        Approve Application with given application_id
        :param application_id: Application ID to approve
        :type application_id: int
        """
        try:
            fetched_application = http.request.env['casemanagement.application'].browse([application_id])
            updated_application = fetched_application.mark_approved().read()

            response = self._response_from_update(application_id, updated_application)

            put_response_to_solid(fetched_application.applicant_id.personal_number,
                                  fetched_application.applicant_id.solid_guid)
        except Exception as e:
            _logger.error(f"Error when approving Application by ID '{application_id}': '{e}'")
            response = Response("{}", content_type="application/json")

        return response

    @http.route('/casemanagement/application/approve/<int:application_id>',
                methods=['GET'],
                auth='user')
    @serialize_exception
    def application_approve_get(self, application_id):
        """
        Approve Application with given application_id
        :param application_id: Application ID to approve
        :type application_id: int
        """
        try:
            fetched_application = http.request.env['casemanagement.application'].browse([application_id])
            fetched_application.mark_approved()

            put_response_to_solid(fetched_application.applicant_id.personal_number,
                                  fetched_application.applicant_id.solid_guid)

            self.set_flash_in_session(
                f"Application approved and VC issued for '{fetched_application.applicant_id.name}'!")

        except Exception as e:
            error_message = f"Error when approving Application by ID '{application_id}': '{e}'"
            _logger.error(error_message)
            self.set_flash_in_session(error_message)

        return request.redirect('/casemanagement')

    @http.route('/casemanagement/application/reject/<int:application_id>',
                methods=['POST'],
                auth='user',
                type='json')
    @serialize_exception
    def application_reject(self, application_id):
        """
        Reject Application with given application_id
        :param application_id: Application ID to reject
        :type application_id: int
        """
        try:
            fetched_application = http.request.env['casemanagement.application'].browse([application_id])
            updated_application = fetched_application.mark_rejected().read()

            response = self._response_from_update(application_id, updated_application)
        except Exception as e:
            _logger.error(f"Error when rejecting Application by ID '{application_id}': '{e}'")
            response = Response("{}", content_type="application/json")

        return response

    @staticmethod
    def _response_from_update(application_id, updated_application):
        """
        Produces a JSON response from updated Application object
        :param application_id: Application ID, int
        :param updated_application: Updated Application object
        :rtype: Response
        """
        # Datetime values can not be serialized out of the box, so odoo provides a serializer
        json_result = json.dumps(updated_application, indent=4, default=date_utils.json_default)
        if not updated_application:
            _logger.error(f"No Application found by ID '{application_id}'")

        return Response(json_result, content_type="application/json")

    @http.route('/casemanagement/applicant/email/<int:applicant_id>',
                methods=['POST'],
                auth='user',
                type='json')
    @serialize_exception
    def email_applicant(self, applicant_id):
        """
        Send email to a given Applicant
        :param applicant_id: Applicant ID to send email to
        :type applicant_id: int
        """
        response_json = dict(status="OK")
        try:
            fetched_applicant = http.request.env['casemanagement.applicant'].browse([applicant_id])
            json_body = http.request.jsonrequest
            if not fetched_applicant:
                response_json = dict(status="error", error=f"No Applicant with ID '{applicant_id}'")
            elif not json_body:
                response_json = dict(status="error", error="Request body cannot be empty")
            elif 'content' not in json_body:
                response_json = dict(status="error", error="Request body should include 'content'")
            else:
                # Mark Applicant approval as 'examine'
                fetched_applicant.update({'approval': 'examine'})
                # Check if there is an attachment, send email
                send_email_with_attachment(fetched_applicant, json_body)
        except Exception as e:
            _logger.error(f"Error when rejecting Application by ID '{applicant_id}': '{e}'")
            response_json = dict(status="error", error=str(e))

        return response_json

    @http.route('/casemanagement/applicant/email/<int:applicant_id>',
                methods=['POST'],
                auth='user',
                website=True)
    @serialize_exception
    def email_applicant_with_attachment(self, applicant_id, **post):
        """
        Send email to a given Applicant. Expected fields are 'subject', 'content' and 'attachment' (optional)
        :param applicant_id: Applicant ID to send email to
        :type applicant_id: int
        """
        response_json = dict(status="OK")
        try:
            fetched_applicant = http.request.env['casemanagement.applicant'].browse([applicant_id])
            if not fetched_applicant:
                response_json = dict(status="error", error=f"No Applicant with ID '{applicant_id}'")
            else:
                # Mark Applicant approval as 'examine'
                fetched_applicant.update({'approval': 'examine'})
                # Check if there is an attachment, send email
                send_email_with_attachment(fetched_applicant, post)
        except Exception as e:
            _logger.error(f"Error when emailing Applicant ID '{applicant_id}': '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")

    @http.route('/casemanagement/<model_name>/email/',
                methods=['POST', 'GET'],
                auth='user',
                website=True)
    @serialize_exception
    def email_with_attachment_example(self, model_name, **post):
        """
        Send email to a given Applicant/Employer.
        Expected fields are 'subject', 'content', 'attachment', 'applicant_id'/'employer_id'
        :param model_name: Model name, one of applicant/employer
        :type model_name: str
        """
        assert model_name in ['applicant', 'employer']

        object_id = int(post.get(f'{model_name}_id', 0))
        objects = http.request.env[f'casemanagement.{model_name}'].search([['email', '!=', None]])
        if not object_id:
            return http.request.render('case_management.send-email',
                                       {'objects': objects,
                                        'model_name': model_name,
                                        'modal_message': self.pop_flash_from_session()})

        try:
            fetched_object = http.request.env[f'casemanagement.{model_name}'].browse([object_id])
            if not fetched_object:
                response_json = dict(status="error", error=f"No {model_name} with ID '{object_id}'")
            else:
                # Check if there is an attachment, send email
                send_email_with_attachment(fetched_object, post)
                return http.request.render('case_management.send-email',
                                           {'objects': objects,
                                            'model_name': model_name,
                                            'modal_message': 'Email sent'})
        except Exception as e:
            _logger.error(f"Error when emailing {model_name} ID '{object_id}': '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")

    @http.route('/casemanagement/vc/',
                methods=['POST', 'GET'],
                auth='user',
                website=True)
    @serialize_exception
    def issue_vc(self, **post):
        issuer = NodeVC(NODE_VC_SCRIPT_PATH)
        result = issuer.issue_vc('890909-0000', '', **post)

        return Response(json.dumps(result), content_type="application/json")

    @http.route('/casemanagement/attachment/download/<int:attachment_id>',
                methods=['GET'],
                auth='user')
    @serialize_exception
    def download_attachment(self, attachment_id):
        try:
            fetched_attachment = http.request.env['casemanagement.attachment'].browse([attachment_id])
            if fetched_attachment:
                binary_data = base64.b64decode(fetched_attachment.base64_data)
                response = request.make_response(
                    binary_data,
                    headers=[('Content-Type', fetched_attachment.mimetype),
                             ('Content-Disposition', content_disposition(fetched_attachment.name)),
                             ('Content-Length', len(binary_data))],
                )
            else:
                response = Response(status=404)
        except Exception as e:
            _logger.error(f"Problem when downloading attachment with ID '{attachment_id}': {e}")
            response = Response(status=404)

        return response

    @http.route('/casemanagement/<model_name>/attach/',
                methods=['POST', 'GET'],
                auth='user',
                website=True)
    @serialize_exception
    def attach_file_to_object(self, model_name, **post):
        """
        Attach file to a given Model (Application/Applicant/Employer).
        Expected fields are 'attachment' and 'application_id/applicant_id/employer_id'
        :param model_name: One of application/applicant/employer
        :type model_name: str
        """
        assert model_name in ['application', 'applicant', 'employer']

        object_id = int(post.get(f'{model_name}_id', 0))
        objects = http.request.env[f'casemanagement.{model_name}'].search([])
        if not object_id:
            return http.request.render('case_management.attach-file',
                                       {'objects': objects,
                                        'model_name': model_name,
                                        'modal_message': self.pop_flash_from_session()})

        try:
            fetched_object = http.request.env[f'casemanagement.{model_name}'].browse([object_id])
            if not fetched_object:
                response_json = dict(status="error", error=f"No {model_name} with ID '{object_id}'")
            else:
                attachment_data = extract_attachment_data(post)
                fetched_object.attach_document(attachment_data.get('name'),
                                               attachment_data.get('binary'),
                                               attachment_data.get('comment_text'))

                return http.request.render('case_management.attach-file',
                                           {'objects': objects,
                                            'model_name': model_name,
                                            'modal_message': 'File attachment saved'})
        except Exception as e:
            _logger.error(f"Error when attaching to {model_name} ID '{object_id}': '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")

    @http.route('/casemanagement/employer/<action>/<int:employer_id>',
                methods=['GET', 'POST'],
                auth='user',
                website=True)
    @serialize_exception
    def approve_employer(self, action, employer_id, **post):
        """
        Approve/Examine a given Employer, save action as comment, email Employer and JobbSprånget
        :param action: 'approve' or 'examine'
        :type action: str
        :param employer_id: Employer ID to approve/examine
        :type employer_id: int
        """
        assert action in ['approve', 'examine']
        try:
            fetched_employer = http.request.env['casemanagement.employer'].search([['id', '=', employer_id]])
            modal_message = ''
            if fetched_employer:
                fetched_employer = fetched_employer[0]
                if post:
                    bar_number = post.get('bar_number')
                    if bar_number:
                        # TODO: possible to validate bar_number with some API?
                        # Save action/attachment as comment
                        attachment_data = extract_attachment_data(post)
                        fetched_employer.attach_document(attachment_data.get('name'),
                                                         attachment_data.get('binary'),
                                                         attachment_data.get('comment_text'))
                        # Approve/Examine Employer
                        approval_state = 'approved' if action == 'approve' else 'examine'
                        fetched_employer.update({'approval': approval_state, 'bar_number': bar_number})

                        modal_message = f'Employer "{fetched_employer.name}" marked "{approval_state}"!'

                        # Email Employer
                        if approval_state == 'approved':
                            email_content = f'Congratulations, {fetched_employer.name}!\n\n' \
                                            f'You have been approved as en employer for internships given via ' \
                                            f'JobbSprånget.\n\n' \
                                            f'This is only for your information and does not require any action.'
                        else:
                            email_content = attachment_data.get('comment_text') or 'no message present'

                        email_subject = 'Employer Approved' if approval_state == 'approved' else 'Employer - Examine'

                        # TODO: email the JobbSprånget team - is it OK to just send a CC here?
                        email_sent = send_email(fetched_employer, email_content, email_subject, cc=JS_EMAIL)
                        if email_sent:
                            modal_message += ' Emails have been sent to Employer and JobbSprånget.'
                        else:
                            modal_message += ' Emails could not be sent!'
                    else:
                        modal_message = f'Employer "{fetched_employer.name}" cannot be approved without BÄR number!'

            return http.request.render('case_management.approve-employer',
                                       {'employer': fetched_employer,
                                        'modal_message': modal_message,
                                        'employer_id': employer_id,
                                        'action': action})
        except Exception as e:
            _logger.error(f"Error when emailing Employer ID '{employer_id}': '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")

    @http.route('/casemanagement/union/sign/<guid>',
                methods=['GET', 'POST'],
                auth='public',
                website=True)
    @serialize_exception
    def union_sign_employer(self, guid, **post):
        """
        Approve a batch of Employer records tied to GUID specific to a particular Union
        :param guid: Unique ID linking a specific Union to a batch of Employer records to approve
        :type guid: str
        """
        try:
            fetched_approvals = http.request.env['casemanagement.approval'].search([
                ('guid', '=', guid),
                ('status', '=', 'waiting')
            ])
            modal_message = ''
            union = 0
            if fetched_approvals:
                union = fetched_approvals[0].union_id
                if post:
                    personal_number = post.get('personal_number')
                    if valid_org_number(personal_number):
                        # Sign/Approve Employers
                        fetched_approvals.sign_approval(personal_number)
                        modal_message = f'Thank you! {len(fetched_approvals)} signed/approved by {personal_number}!'
                    else:
                        modal_message = 'Signature failed: invalid personal number!'
            else:
                modal_message = 'No Employers found to be signed. Someone already signed?'

            return http.request.render('case_management.sign-employers',
                                       {'approvals': fetched_approvals,
                                        'modal_message': modal_message,
                                        'union': union,
                                        'guid': guid})
        except Exception as e:
            _logger.error(f"Error when processing Union Approval with guid '{guid}': '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")

    @http.route('/casemanagement/union/csv/<guid>',
                methods=['GET'],
                auth='public')
    def union_csv_download_employers(self, guid):
        """
        Download CSV of Employer records corresponding to the GUID of a particular Union
        :param guid: Unique ID linking a specific Union to a batch of Employer records to approve
        :type guid: str
        """
        try:
            fetched_approvals = http.request.env['casemanagement.approval'].search([
                ('guid', '=', guid),
                ('status', '=', 'waiting')
            ])
            employers = [approval.employer_id for approval in fetched_approvals]
            binary_data = http.request.env['casemanagement.employer'].employers_to_csv(employers)
            content_type = 'text/csv;charset=utf8'
            union_name = fetched_approvals[0].union_id.name if fetched_approvals else ''

            return request.make_response(
                binary_data,
                headers=[('Content-Type', content_type),
                         ('Content-Disposition', content_disposition(f'employers-{union_name}-{guid}.csv')),
                         ('Content-Length', len(binary_data))],
            )

        except Exception as e:
            _logger.error(f"Error when processing Union CSV download with guid '{guid}': '{e}'")
            return Response(status=400)

    @http.route('/casemanagement/unions/update',
                methods=['GET', 'POST'],
                auth='user')
    @serialize_exception
    def unions_update(self, **post):
        """
        Update Unions' emails
        """
        try:
            union_model = http.request.env['casemanagement.union']
            fetched_unions = union_model.search([])
            modal_message = ''
            if fetched_unions:
                if post:
                    # Update email addresses for Unions
                    updated_unions = union_model.update_email(**post)
                    if updated_unions:
                        modal_message = f'Thank you! {len(updated_unions)} Unions were updated!'
            else:
                modal_message = 'No Unions found to be updated!'

            return http.request.render('case_management.update-unions',
                                       {'unions': fetched_unions,
                                        'modal_message': modal_message
                                        })
        except Exception as e:
            _logger.error(f"Error when processing Union updates: '{e}'")
            response_json = dict(status="error", error=e)

        return Response(response_json, content_type="application/json")


def _validate_notification_signature(signed_token):
    """
    Validate signed_token delivered along with resource notification from Solid Pod server
    :param signed_token: Serialized JWT, signed by Solid Pod server public key
    :type signed_token: str
    :rtype: bool
    """
    try:
        pod_server_metadata = requests.get(SOLID_SERVER_URL + "/.well-known/solid").json()

        _logger.info("================= pod_server_metadata =============================================")
        _logger.info(pod_server_metadata)

        jwks_endpoint = pod_server_metadata.get('jwks_endpoint')
        pod_server_keys = requests.get(jwks_endpoint).json()

        _logger.info("================= pod_server_keys =============================================")
        _logger.info(pod_server_keys)

        # Use the whole JWKS when verifying signed JWT
        pod_server_key_set = jwcrypto.jwk.JWKSet.from_json(json.dumps(pod_server_keys))

        # Claims can be checked directly when deserializing JWT
        deserialized_token = jwcrypto.jwt.JWT(check_claims=dict(iss=SOLID_SERVER_URL))
        deserialized_token.deserialize(signed_token, key=pod_server_key_set)

        _logger.info("================= authorization_jwt =============================================")
        _logger.info(signed_token)
        _logger.info("================= deserialized_token claims =====================================")
        _logger.info(deserialized_token.claims)
        _logger.info("=================================================================================")

        # Value of "iss" has to match SOLID_SERVER_URL according to Solid Notifications spec:
        # https://github.com/solid/notifications/blob/main/webhook-subscription.md#10-webhook-request-with-token-signed-by-the-pod-key
        claim_to_check = json.loads(deserialized_token.claims).get('iss')

    # TODO: catch specific validation exceptions? And do what?
    except Exception as e:
        claim_to_check = None
        _logger.error(f"Could not validate signed token '{signed_token}', error: '{e}'")

    return claim_to_check and claim_to_check == SOLID_SERVER_URL


def validate_campaign_date_end(campaign_end):
    """
    Validates campaign end date - must parse and be in the future
    :param campaign_end: Date string in format YYYY-MM-DD
    :type campaign_end: str
    :rtype: datetime.datetime | bool
    """
    parsing_format = '%Y-%m-%d'
    try:
        parsed_datetime = datetime.datetime.strptime(campaign_end, parsing_format)
    except Exception as e:
        _logger.error(f'Cannot parse string "{campaign_end}" as datetime using format "{parsing_format}" '
                      f'with error: "{e}"')
        return False

    if datetime.datetime.now() > parsed_datetime:
        return False

    return parsed_datetime


def process_employers(import_wizard, fields, options):
    """
    Updates existing Employer records with new data from CSV file that failed to create new records
    Creates new Employer records if they did not exist before
    (update is not supported with CSV import out of the box)
    :param import_wizard: Import wizard used to process CSV imports (instance of 'base_import.import' model)
    :param fields: CSV import fields
    :type fields: list
    :param options: CSV import options
    :type options: dict
    :return: Updated Employer recordset
    """
    updated_employers = list()

    try:
        # Use built-in odoo tools for CSV import
        input_file_data, import_fields = import_wizard._convert_import_data(fields, options)
        import_fields, data = import_wizard._handle_multi_mapping(import_fields, input_file_data)

        parsed_employers = convert_data_to_employers(import_fields, data)
        for e in parsed_employers:
            # Find existing record by org_number
            domain = [('org_number', '=', e['org_number'])]
            existing_employer = http.request.env['casemanagement.employer'].search(domain)
            if existing_employer:
                # Update existing record with new data e
                existing_employer[0].update(e)
                updated_employers.append(existing_employer[0])
            else:
                # Create new ones
                created_employer = http.request.env['casemanagement.employer'].create(e)
                updated_employers.append(created_employer)
    except Exception as e:
        _logger.error(f'Error when updating/creating new Employers from CSV import: "{e}"')

    return updated_employers


def convert_data_to_employers(import_fields, data):
    """
    Converts parsed data to list of Employer dicts
    :param import_fields: Import fields in the same order as corresponding data
    :type import_fields: list
    :param data: Data in the same order as import_fields
    :type data: list[list]

    :return: List of dicts that can be used to update existing Employer records
    :rtype: list
    """
    parsed_employers = list()

    _logger.debug('========================= convert_data_to_employers ==========================')
    _logger.debug(json.dumps(import_fields, indent=4))
    _logger.debug(json.dumps(data, indent=4))
    _logger.debug('==============================================================================')

    if import_fields and data:
        for r in data:
            employer = dict()
            for i in range(len(import_fields)):
                # Make sure only name/org_number/email columns are imported - only these are allowed in CSV import
                if import_fields[i] in ['name', 'org_number', 'email']:
                    employer[import_fields[i]] = r[i]

            # Mark Employer to be examined because of possible update
            employer['approval'] = 'examine'

            parsed_employers.append(employer)

    _logger.debug(json.dumps(parsed_employers, indent=4))

    return parsed_employers


def get_local_datetime(utc_dt_object):
    """
    Converts from UTC to local Stockholm timezone
    :param utc_dt_object: Datetime object to convert
    :type utc_dt_object: datetime.datetime
    :rtype: datetime.datetime
    """
    # TODO: Get rid of this conversion and save in correct timezone from the beginning?
    # Represent in local timezone, stored as naive UTC
    from_z = tz.tzutc()
    to_z = tz.gettz('Europe/Stockholm')
    # Tell the datetime object that it's in UTC, convert timezone, format
    return utc_dt_object.replace(tzinfo=from_z).astimezone(to_z)
