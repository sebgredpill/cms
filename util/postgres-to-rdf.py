# -*- coding: utf-8 -*-
import psycopg2

import rdflib
from rdflib import Graph
from rdflib.namespace import DC, FOAF, DOAP, RDF, RDFS, XSD
from rdflib import URIRef, Literal


class Applicant:
    id = None
    name = None
    employer = None
    employer_good = None

    registration_date = None

    decision_type = None
    decision_type_good = None

    residence_permit_type = None
    residence_permit_type_good = None

    origin_country = None

    auto_suggestion = None

    approval = None

    def __init__(self, *args, **kwargs):
        for k in kwargs.keys():
            self.__setattr__(k, kwargs[k])

    def to_rdf(self):

        g = Graph()

        self_keys = [k for k in dir(self) if not k.startswith("__") and not callable(getattr(self, k))]

        print(self_keys)

        print("vars %s" % self.__dict__)

        self_dict = self.__dict__

        for k in self_dict:
            # g.add((
            #     "http://af.com/jobspranget/applicant",
            #     k,
            #     Literal(self.k, datatype=XSD.string)
            # ))
            print(k, self_dict[k])

            ## Byt till användarens pod namn
            key_string = "http://af.com/jobspranget/applicant#%s"

            g.add((
                URIRef("http://af.com/jobspranget/applicant"),
                URIRef(key_string % k),
                Literal(self_dict[k], datatype=XSD.string)
            ))

        return g


def load_10_applicants():
    connection = None

    try:
        connection = psycopg2.connect(user="odooapp",
                                      password="odoo-app",
                                      host="localhost",
                                      port="5432",
                                      database="odooapp")
        cursor = connection.cursor()
        postgreSQL_select_Query = "select * from casemanagement_applicant limit 10"

        cursor.execute(postgreSQL_select_Query)
        print("Selecting rows from mobile table using cursor.fetchall")
        applicant_records = cursor.fetchall()

        print("Print each row and it's columns values")
        ten_applicants = []
        for row in applicant_records:
            print("Id = ", row[0], )
            print("Name = ", row[1])
            print("Employer  = ", row[2], "\n")
            print("Employer_good  = ", row[3], "\n")
            print("Registration_date  = ", row[15], "\n")
            print("Residence_permit_type  = ", row[6], "\n")
            print("Residence_permit_type_good  = ", row[7], "\n")
            print("Origin_country  = ", row[9], "\n")
            print("Auto_suggestion  = ", row[10], "\n")
            print("Approval  = ", row[11], "\n")

            constructor_args = {
                "id": row[0],
                "name": row[1],
                "employer": row[2],
                "employer_good": row[3],
                "registration_date": row[15],
                "residence_permit_type": row[6],
                "residence_permit_type_good": row[7]
            }

            applicanty = Applicant(**constructor_args)
            ten_applicants.append(applicanty)

    except (Exception, psycopg2.Error) as error:
        print("Error while fetching data from PostgreSQL", error)

    finally:
        # closing database connection.
        if connection:
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

    return ten_applicants


def main():
    g = Graph()
    g.parse("http://dbpedia.org/resource/Semantic_Web")

    for s, p, o in g:
        print(s, p, o)

    g2 = Graph()

    g2.add((
        URIRef("http://example.com/person/nick"),
        FOAF.givenName,
        Literal("Nick", datatype=XSD.string)
    ))

    for s, p, o in g2:
        print(s, p, o)

    print(g2.serialize(format="turtle"))

    ten_applicants = load_10_applicants()

    one_app = ten_applicants[0]

    print(one_app.to_rdf().serialize(format="turtle"))


main()
