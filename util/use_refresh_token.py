#!/bin/python3
import json

import requests
import base64
import jwcrypto
# import jwcrypto.jwk

#import jwcrypto.jwt
import os
import re
import datetime

from jwcrypto import jwk, jwt


REFRESH_TOKEN="nQWLgpqcPrhF6xfGfdEoE2lzA8eVLtYf_SIOXtIOKB1"
CLIENT_ID="Q-bB7PgeWZMmp7c0_jtEP"

token = {
  "refreshToken" : "nQWLgpqcPrhF6xfGfdEoE2lzA8eVLtYf_SIOXtIOKB1",
  "clientId"     : "Q-bB7PgeWZMmp7c0_jtEP",
  "clientSecret" : "bjYQYT5P-9zuXwFldxmyrYbofSSZhQqXB-4g2-WpBOw4JGwqfHVWa4jsYQ5m_7We5Imkkq3Y9Be8zZqHdn3r3w",
  "oidcIssuer"   : "http://localhost:3000/"
}

with open('refresh-token.json', 'r') as token_file:
    token = json.loads(token_file.read())

print(token)

def make_random_string():
    x = base64.urlsafe_b64encode(os.urandom(40)).decode('utf-8')
    x = re.sub('[^a-zA-Z0-9]+', '', x)
    return x

def make_token_for(jwk, uri, method):
    print(jwk)
    jwt = jwcrypto.jwt.JWT(header={
        "typ": "dpop+jwt",
        "alg": "ES256",
        "jwk": jwk.export(private_key=False, as_dict=True)
        },
                           claims={
                               "jti": make_random_string(),
                               "htm": method,
                               "htu": uri,
                               "iat": int(datetime.datetime.now().timestamp())
                           })

    jwt.make_signed_token(jwk)
    return jwt.serialize()

def make_headers(access_token, jwk, url, method):
    keypair = None

    headers = {
        "Authorization": ("DPoP " + access_token),
        "DPoP": make_token_for(jwk, url, method)
    }

    return headers

def main():


    url = token["oidcIssuer"] + ".well-known/openid-configuration"

    req = requests.get(url=url)

    config_response = json.loads(req.text)

    print(req.status_code)
    print(req.text)

    key_url = token["oidcIssuer"] + "idp/jwks"

    key_request = requests.get(url=key_url)

    token_jwk = None

    if key_request.status_code == 200:
        print(key_request.text)
        keys = json.loads(key_request.text)
        #         print(keys["keys"][0])
        print(keys["keys"][0])

        token_jwk = jwk.JWK.from_json(json.dumps(keys["keys"][0]))
        print("loaded key")

    token_endpoint = config_response["token_endpoint"] 

    clientidsecrethashed = base64.b64encode((token["clientId"] + ":" + token["clientSecret"]).encode('utf-8'))

    token_headers = {
            "Authorization": "Basic %s" % clientidsecrethashed.decode('utf-8')
    }

    token_req = requests.post(url=token_endpoint,
                              data={
                                  "'gragoobla": "refresh_token",
                                  "refresh_token": token["refreshToken"],
                                  "grant_type": "refresh_token"
                              },
                              headers=token_headers
    )

    print(token_req.url)

    print("token_req status %s" % (token_req.status_code))
    print(token_req.text)

    new_token = None
    access_token = None

    if token_req.status_code == 200:
        
        new_token = token_req.json()
        
        print(new_token)
        id_token = new_token['id_token'] if 'id_token' in new_token else None
        access_token = new_token['access_token'] if 'access_token' in new_token else None

        
        print("id token %s\naccess token %s" % (id_token, access_token))

        print(type(id_token))
        print(type(id_token.encode('utf-8')))

        id_token_jwt = jwt.JWT(jwt=id_token, key=token_jwk)

        print(id_token_jwt)
        print(id_token_jwt.claims)
        claims_loaded = json.loads(id_token_jwt.claims)
        print("Token came from user %s" % claims_loaded["webid"])
        print(dir(id_token_jwt))

    profile_url = "http://localhost:3000/source-test-se/profile/"
    print(token_jwk)

    headers = make_headers(access_token, token_jwk, url, 'GET')
    
    return 0

main()
