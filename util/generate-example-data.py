# -*- coding: utf-8 -*-
import psycopg2
from random import randrange

# TODO: use some config file to contain variables below?
db_host = "db"
db_port = 5432
db_user = "odooapp"
db_password = "odoo-app"
db_database = "odooapp"
db_table_name = "casemanagement_applicant"


def random_between(low, high):
    return randrange(high)


def generate_applicants(amount=10):
    countries_list = ["Indien", "Iran", "Syrien", "Vietnam", "Kina",
                      "Pakistan", "Andorra"]

    first_name_list = ["Mary", "John", "Lisa"]
    surname_list = ["Marchall", "Johnson", "Persson"]
    decision_types_list = ["CG", "A3", "DA", "C0", "D", "E"]

    residence_permit_type_list = ["put", "t-uat", "t-ut", "ut", "uk"]

    auto_suggestions_list = ["Ok", "Oklar", "Nej"]

    applicants = []

    for n in range(0, amount):
        applicants.append({
            "first_name": first_name_list[random_between(0, len(first_name_list))],
            "last_name": surname_list[random_between(0, len(surname_list))],
            "residence_permit_type": residence_permit_type_list[random_between(0, len(residence_permit_type_list))],
            "residence_permit_type_good": randrange(2) == 1,
            "origin_country": countries_list[random_between(0, len(countries_list))],
            "auto_suggestion": auto_suggestions_list[random_between(0, len(auto_suggestions_list))],
            "approval": randrange(2) == 1

        })

    return applicants


def main():
    conn = psycopg2.connect(host=db_host,
                            port=db_port,
                            user=db_user,
                            password=db_password,
                            database=db_database)

    cursor = conn.cursor()

    cursor.execute("SELECT 1")
    # conn.commit()

    records = cursor.fetchall()

    if records[0][0] == 1:
        print("Could ping database")

    applicants = generate_applicants(100)

    print(applicants)

    for applicant in applicants:
        query = """INSERT INTO %s (first_name,
        last_name,
        residence_permit_type,
        residence_permit_type_good,
        origin_country,
        approval)
        VALUES ('%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s')""" % (db_table_name,
                    applicant["first_name"],
                    applicant["last_name"],
                    applicant["residence_permit_type"],
                    applicant["residence_permit_type_good"],
                    applicant["origin_country"],
                    applicant["approval"])
        print(query)
        cursor.execute(query)

    conn.commit()

    cursor.close()
    conn.close()

    print("true")
    return True


main()
