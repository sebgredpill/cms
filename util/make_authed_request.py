

import requests
import jwcrypto
import jwcrypto.jwk
import jwcrypto.jwt
import json
import base64
import os
import re
import datetime

def make_random_string():
    x = base64.urlsafe_b64encode(os.urandom(40)).decode('utf-8')
    x = re.sub('[^a-zA-Z0-9]+', '', x)
    return x


def make_token_for(keypair, uri, method):
    print(keypair)
    jwt = jwcrypto.jwt.JWT(header={
        "typ":
        "dpop+jwt",
        "alg":
        "ES256",
        "jwk":
        keypair.export(private_key=False, as_dict=True)
    },
                           claims={
                               "jti": make_random_string(),
                               "htm": method,
                               "htu": uri,
                               "iat": int(datetime.datetime.now().timestamp())
                           })
    jwt.make_signed_token(keypair)
    return jwt.serialize()

    

def make_headers(url, method):
    with open('sensitivedata.dat', 'r') as stored_keys:
        json_loaded = json.loads(stored_keys.read())
        keypair = jwcrypto.jwk.JWK.from_json(json_loaded["key"])
        access_token = json_loaded["access_token"]
        headers = {
            "Authorization": ("DPoP " + access_token),
            "DPoP": make_token_for(keypair, url, method)
        }
        return headers

def main():
    url = "http://localhost:3000/source-test-se/profile/"

    headers = make_headers(url, 'GET')
    print(headers)

    resp = requests.get(url, headers=headers)

    print(resp.status_code)
    print(resp.text)

#    print(resp.json())
main()
